import _cloneDeep from 'lodash/cloneDeep';

export default {
    methods: {
        get(obj, path, defaultValue = undefined) {
            const travel = regexp => String.prototype.split
                .call(path, regexp)
                .filter(Boolean)
                .reduce((res, key) => (res !== null && res !== undefined ? res[key] : res), obj);
            const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);
            return result === undefined || result === obj ? defaultValue : result;
        },
        numberWithSpaces(x) {
            if (!x) { return x; }
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
        },
        getCompareLength(arr) {
            const array = [];
            if (arr) {
                arr.forEach(item => {
                    if (item.compare === true) {
                        array.push(item);
                    }
                });
            }
            return array.length;
        },
        isEmptyObject(value) {
            return Object.keys(value).length === 0 && value.constructor === Object;
        },
        isNullObject(arr, type) {
            let counter = 0;
            arr.forEach(item => {
                if (item[type] !== null) {
                    counter++;
                }
            });
            return counter;
        },
        parseArray(value, path) {
            const arr = [];
            value.forEach(item => {
                if (this.get(item, path)) {
                    arr.push(item);
                }
            });
            return arr;
        },
        loadImages(sources, callback) {
            const images = {};
            let loadedImages = 0;
            let numImages = 0;
            // eslint-disable-next-line no-unused-vars
            for (const src in sources) {
                numImages++;
            }
            for (const src in sources) {
                images[src] = new Image();
                images[src].onload = function () {
                    if (++loadedImages >= numImages) {
                        callback(images);
                    }
                };
                images[src].src = sources[src];
            }
        },
        getCategory(type) {
            let setCategory = null;
            switch (type) {
            case 'washing':
                setCategory = [1, 2];
                break;
            case 'mixer':
                setCategory = 3;
                break;
            case 'garbage':
                setCategory = 4;
                break;
            default:
                setCategory = [5, 6, 7];
            }

            return setCategory;
        },
        parseCategory(id) {
            let setCategory = null;
            switch (id) {
            case 1:
            case 2:
                setCategory = 'washing';
                break;
            case 3:
                setCategory = 'mixer';
                break;
            case 4:
                setCategory = 'garbage';
                break;
            default:
                setCategory = 'accessories';
            }

            return setCategory;
        },
        parseFiltersForParams(items, type) {
            const params = [];
            let obj = {
                category: this.getCategory(type),
            };
            Object.entries(items[type]).forEach(([key, value]) => {
                if (value !== 'none') {
                    if (key === 'price') {
                        let priceGte = value.split('>');
                        let priceLte = value.split('<');
                        let priceGtLt = value.split('!');
                        if (priceLte.length === 2) {
                            priceLte = priceLte.filter(item => item);
                            obj = {
                                ...obj,
                                price_lte: priceLte,
                            };
                        }
                        if (priceGte.length === 2) {
                            priceGte = priceGte.filter(item => item);
                            obj = {
                                ...obj,
                                price_gte: priceGte,
                            };
                        }
                        if (priceGtLt.length === 3) {
                            priceGtLt = priceGtLt.filter(item => item);
                            obj = {
                                ...obj,
                                price_lt: priceGtLt[1],
                                price_gt: priceGtLt[0],
                            };
                        }
                    }
                    if (key === 'materialType' || key === 'color') {
                        obj = {
                            ...obj,
                            [key]: value,
                        };
                    }
                    if (type === 'washing' && (key !== 'price' && key !== 'materialType')) {
                        obj = {
                            ...obj,
                            [`ttcBowl.${key}`]: value,
                        };
                    }
                    if (type === 'mixer' && (key !== 'color' && key !== 'price')) {
                        obj = {
                            ...obj,
                            [`ttcMixer.${key}`]: value,
                        };
                    }
                    if (type === 'garbage') {
                        obj = {
                            ...obj,
                            [`ttcGarbageSystem.${key}`]: value,
                        };
                    }
                    if (type === 'accessories') {
                        obj = {
                            ...obj,
                            category: value,
                        };
                    }
                }
            });
            params.push(obj);
            return params;
        },
        parseFilterForFetch(key, value, type, _limit = -1) {
            const params = [];
            let obj = {
                category: this.getCategory(type),
                _limit,
            };
            if (value !== 'none') {
                if (key === 'price') {
                    const compatibility = {
                        washing: {
                            0: [0, 1000000],
                            1: [0, 20000],
                            2: [20000, 30000],
                            3: [30000, 1000000],
                        },
                        mixer: {
                            0: [0, 1000000],
                            1: [0, 10000],
                            2: [10000, 20000],
                            3: [20000, 1000000],
                        },
                    };

                    obj = {
                        ...obj,
                        price_gte: compatibility[type][value][0],
                        price_lt: compatibility[type][value][1],
                    };
                }
                if (key === 'color') {
                    obj = {
                        ...obj,
                        [key]: value,
                    };
                }
                if (type === 'washing' && (key !== 'price') && (key !== 'color')) {
                    if (key === 'sizeCupboard') { // TODO пока не до конца разобрался с запросами, нужен рефакторинг "собирания" объекта
                        obj = {
                            ...obj,
                            ttcBowl: {
                                [key]: {
                                    size: value,
                                },
                            },
                        };
                    } else {
                        obj = {
                            ...obj,
                            [`ttcBowl[${key}][0]`]: value,
                        };
                    }
                }
                if (type === 'mixer' && (key !== 'color' && key !== 'price')) {
                    obj = {
                        ...obj,
                        [`ttcMixer[${key}][0]`]: value,
                    };
                }
                if (type === 'garbage') {
                    obj = {
                        ...obj,
                        [`ttcGarbageSystem[${key}][0]`]: value,
                    };
                }
                if (type === 'accessories') {
                    obj = {
                        ...obj,
                        category: value,
                    };
                }
            }
            params.push(obj);
            return params;
        },
        parseObjectToArray(value) {
            const copied = _cloneDeep(value);
            const arr = [];
            Object.entries(copied).forEach(([key, val]) => {
                if (val && key !== 'filters' && key !== 'canvasImg' && key !== 'compare') {
                    arr.push(val);
                }
            });
            return arr;
        },
        parseObject(value, path) {
            const arr = [];
            Object.entries(value).forEach(([key, val]) => {
                if (this.get(val, path)) {
                    arr.push(val);
                };
            });
            return arr;
        },
        parseUrlPictures(value, path) {
            if (this.get(value, path)) {
                return this.$axios.defaults.baseURL + this.get(value, path);
            }
            return false;
        },
        parseDescText(item) {
            return this.$md.render(item).split('<ul')[0];
        },
        isBoolean(item) {
            if (typeof item === 'boolean') {
                return true;
            }
            return false;
        },
        isTrueBoolean(item) {
            return !item;
        },
        parseCardValue(item, list) {
            if (typeof item !== 'boolean' && typeof item !== 'object' && typeof item !== 'number') {
                return list[item];
            }
            if (typeof item === 'object' && item !== null) {
                return list[item.size];
            }
            if (typeof item === 'boolean') {
                return item ? 'Да' : 'Нет';
            }
            return item;
        },
        removeNullValueFromObject(arr) {
            Object.entries(arr).forEach(([key, val]) =>
                (val === null || val === '') && delete arr[key],
            );
            return arr;
        },
        preOpenFilters(content, state, selected) {
            Object.entries(content).forEach(([key, val]) => {
                val.map(item => {
                    if (state[key] && state[key][item.groupId] !== 'none' && selected[key]) {
                        item.active = true;
                    }
                    return item;
                });
            });
        },
        getSumm(items, discount) {
            let sum = 0;
            items.forEach(item => {
                sum += item.price;
            });
            if (discount) {
                sum -= discount;
            }
            if (sum < 0) {
                return 0;
            }
            return sum;
        },
        getSummFromObject(items, discount) {
            let sum = 0;
            Object.entries(items).forEach(([key, val]) => {
                if (val && key !== 'filters' && key !== 'canvasImg' && key !== 'compare') {
                    if (val.price) {
                        sum += val.price;
                    }
                }
            });
            if (discount) {
                sum -= discount;
            }
            if (sum < 0) {
                return 0;
            }
            return sum;
        },
        pureKeys(items) {
            const array = [];
            if (items) {
                Object.entries(items).forEach(([key, value]) => {
                    if (!(key === 'id' || value === null)) {
                        array.push(key);
                    }
                });
            }
            return array;
        },
        parseArrayWithUniqueName(array) {
            return array.reduce(function (acc, curr) {
                const findIfNameExist = acc.findIndex(function (item) {
                    return item.name === curr.name;
                });
                if (findIfNameExist === -1) {
                    const obj = {
                        name: curr.name,
                        value: [curr],
                        isShowImage: false,
                    };
                    acc.push(obj);
                } else {
                    curr.isShowImage = false;
                    acc[findIfNameExist].value.push(curr);
                }
                return acc;
            }, []);
        },
    },
};
