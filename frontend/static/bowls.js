/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable quote-props */
export default [
    {
        "Article": "526006",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "516918",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "516919",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "516922",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "516923",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "516924",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "516927",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "517411",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "518932",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "520624",
        "Family": "ZA",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15400"
    },
    {
        "Article": "526035",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "526032",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523802",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523709",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523710",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523805",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523711",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523808",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523712",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523809",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523713",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523810",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523714",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523812",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523720",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523835",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523725",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "523838",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39000"
    },
    {
        "Article": "525879",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524814",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524815",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524816",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524817",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524818",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524819",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524821",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524823",
        "Family": "EO",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "525869",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "517156",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "517157",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "517160",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "517161",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "517162",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "517165",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "517317",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "518846",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "520543",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17200"
    },
    {
        "Article": "525914",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "513027",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "513028",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "513029",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "513035",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "513932",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "515038",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "517345",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "518868",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "520566",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23000"
    },
    {
        "Article": "525311",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26900"
    },
    {
        "Article": "525913",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519572",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519573",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519574",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519576",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519577",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519578",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519580",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "519581",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "520570",
        "Family": "MA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21200"
    },
    {
        "Article": "526013",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "514725",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "514726",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "514727",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "514728",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "514732",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "515070",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "517416",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "518937",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "520627",
        "Family": "ZA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18800"
    },
    {
        "Article": "526009",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524721",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524722",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524723",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524724",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524725",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524726",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524727",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524728",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "524730",
        "Family": "ZA",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15800"
    },
    {
        "Article": "525962",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "515670",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "515671",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "515672",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "515676",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "515679",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "517392",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "518915",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "520604",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16100"
    },
    {
        "Article": "525918",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "513036",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "513037",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "513038",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "513044",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "515041",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "517348",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "518871",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "520571",
        "Family": "MA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30300"
    },
    {
        "Article": "526016",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520511",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520512",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520513",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520514",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520515",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520516",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520517",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520518",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "520519",
        "Family": "ZA",
        "Cabinet size": "50S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "25900"
    },
    {
        "Article": "525951",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521504",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521669",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521670",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521671",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521672",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521673",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521675",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "521677",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28500"
    },
    {
        "Article": "525304",
        "Family": "PL",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "33200"
    },
    {
        "Article": "525871",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518521",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518522",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518524",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518525",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518526",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518528",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518529",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "518848",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "520544",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20500"
    },
    {
        "Article": "525851",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523462",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523463",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523464",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523465",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523466",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523467",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523469",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "523471",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "525850",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523472",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523473",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523474",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523476",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523477",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523478",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523480",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "523482",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "525849",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524643",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524644",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524645",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524646",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524647",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524648",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524650",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "524652",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "76400"
    },
    {
        "Article": "525848",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524653",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524654",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524655",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524656",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524657",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524658",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524660",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "524662",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65600"
    },
    {
        "Article": "525858",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523500",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523501",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523502",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523503",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523504",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523505",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523507",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "523509",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "68500"
    },
    {
        "Article": "525857",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523510",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523511",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523512",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523513",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523514",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523515",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523517",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "523519",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "57600"
    },
    {
        "Article": "525837",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525347",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525348",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525349",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525343",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525344",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525346",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525351",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "525298",
        "Family": "AO",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "65000"
    },
    {
        "Article": "526339",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45800"
    },
    {
        "Article": "525890",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524539",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524540",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524541",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524542",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524543",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524544",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524546",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "524548",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42600"
    },
    {
        "Article": "525300",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "49500"
    },
    {
        "Article": "525060",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "525052",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "525056",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "525048",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "525063",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "525055",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "525059",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "525051",
        "Family": "SI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "35400"
    },
    {
        "Article": "526060",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "526057",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523944",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523945",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523946",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523947",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523948",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523949",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523951",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523974",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523975",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523976",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523977",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523978",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523979",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523981",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523953",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "523983",
        "Family": "ZE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "52300"
    },
    {
        "Article": "526050",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523706",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523707",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523708",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523757",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523758",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523759",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523761",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "523763",
        "Family": "ZE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "44200"
    },
    {
        "Article": "525838",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521755",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521756",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521759",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521760",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521761",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521762",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521764",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521765",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "521766",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "81500"
    },
    {
        "Article": "521767",
        "Family": "AR",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "81500"
    },
    {
        "Article": "525882",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524834",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524835",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524836",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524837",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524838",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524839",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524841",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "524843",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "29400"
    },
    {
        "Article": "525922",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "516156",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "516157",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "516158",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "516162",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "516165",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "517351",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "518874",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "520574",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23400"
    },
    {
        "Article": "525926",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "513045",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "513046",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "513047",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "513053",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "515045",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "517354",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "518877",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "520577",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "41900"
    },
    {
        "Article": "525313",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "48400"
    },
    {
        "Article": "525925",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "513468",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "513469",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "513473",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "513553",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "513938",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "515044",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "517353",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "518876",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "520576",
        "Family": "MA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "21900"
    },
    {
        "Article": "526019",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523273",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523274",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523275",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523276",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523277",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523278",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523279",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523280",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "523282",
        "Family": "ZA",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "18400"
    },
    {
        "Article": "525931",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "515279",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "515280",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "515281",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "515286",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "515287",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "517360",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "518881",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "520581",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "525315",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "42200"
    },
    {
        "Article": "525953",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521678",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521679",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521681",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521682",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521683",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521684",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521686",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "521688",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "31500"
    },
    {
        "Article": "525306",
        "Family": "PL",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "36500"
    },
    {
        "Article": "525955",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521689",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521690",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521691",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521692",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521693",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521694",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521696",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "521698",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34200"
    },
    {
        "Article": "525308",
        "Family": "PL",
        "Cabinet size": "60L",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "39600"
    },
    {
        "Article": "525873",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "514194",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "514197",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "514198",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "514199",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "514592",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "515066",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "517320",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "518850",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "520545",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20600"
    },
    {
        "Article": "526021",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "514741",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "514742",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "514743",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "514744",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "514748",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "515072",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "517419",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "518940",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "520632",
        "Family": "ZA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "30800"
    },
    {
        "Article": "526024",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "517568",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "517569",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "517571",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "517572",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "517573",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "517576",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "517577",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "518943",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "520635",
        "Family": "ZA",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27300"
    },
    {
        "Article": "525893",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525187",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525188",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525189",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525190",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525191",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525192",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525194",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525196",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "46200"
    },
    {
        "Article": "525302",
        "Family": "ET",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "53500"
    },
    {
        "Article": "525885",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524860",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524861",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524862",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524863",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524864",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524865",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524867",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "524869",
        "Family": "EO",
        "Cabinet size": "80X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "32800"
    },
    {
        "Article": "525877",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "516629",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "516630",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "516633",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "516634",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "516635",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "516638",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "517323",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "518852",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "520546",
        "Family": "DA",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "525906",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524960",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524961",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524962",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524963",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524964",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524965",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524967",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "524969",
        "Family": "LX",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "34900"
    },
    {
        "Article": "525867",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523656",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523657",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523658",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523659",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523660",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523662",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523666",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "523669",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "525938",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "515567",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "515568",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "515569",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "515573",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "515576",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "517367",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "518888",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "520588",
        "Family": "MA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "51200"
    },
    {
        "Article": "526031",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "514757",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "514758",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "514759",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "514760",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "514763",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "514764",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "515074",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "518950",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "520642",
        "Family": "ZA",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "23800"
    },
    {
        "Article": "526029",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "516677",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "516678",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "516679",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "516680",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "516683",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "516686",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "517424",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "518948",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "520640",
        "Family": "ZA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "27500"
    },
    {
        "Article": "525936",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "513268",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "513269",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "513270",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "513273",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "515050",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "517364",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "518886",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "520586",
        "Family": "MA",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "40500"
    },
    {
        "Article": "526110",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526101",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526102",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526103",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526104",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526105",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526106",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526107",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526108",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "83900"
    },
    {
        "Article": "526109",
        "Family": "VT",
        "Cabinet size": "90X",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "96900"
    },
    {
        "Article": "526077",
        "Family": "EN",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15200"
    },
    {
        "Article": "513801",
        "Family": "EN",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15200"
    },
    {
        "Article": "514230",
        "Family": "EN",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15200"
    },
    {
        "Article": "513796",
        "Family": "EN",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15200"
    },
    {
        "Article": "515080",
        "Family": "EN",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15200"
    },
    {
        "Article": "513799",
        "Family": "EN",
        "Cabinet size": "40S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "15200"
    },
    {
        "Article": "526100",
        "Family": "TM",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16900"
    },
    {
        "Article": "521392",
        "Family": "TM",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16900"
    },
    {
        "Article": "521393",
        "Family": "TM",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16900"
    },
    {
        "Article": "521394",
        "Family": "TM",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16900"
    },
    {
        "Article": "521395",
        "Family": "TM",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16900"
    },
    {
        "Article": "521390",
        "Family": "TM",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16900"
    },
    {
        "Article": "526083",
        "Family": "LE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "522203",
        "Family": "LE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "522201",
        "Family": "LE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "522204",
        "Family": "LE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "522205",
        "Family": "LE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "522206",
        "Family": "LE",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "526095",
        "Family": "RI",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "14900"
    },
    {
        "Article": "521398",
        "Family": "RI",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "14900"
    },
    {
        "Article": "521399",
        "Family": "RI",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "14900"
    },
    {
        "Article": "521400",
        "Family": "RI",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "14900"
    },
    {
        "Article": "521401",
        "Family": "RI",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "14900"
    },
    {
        "Article": "521396",
        "Family": "RI",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "14900"
    },
    {
        "Article": "518185",
        "Family": "FA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17600"
    },
    {
        "Article": "518188",
        "Family": "FA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17600"
    },
    {
        "Article": "521405",
        "Family": "FA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17600"
    },
    {
        "Article": "518186",
        "Family": "FA",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17600"
    },
    {
        "Article": "526085",
        "Family": "LE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17800"
    },
    {
        "Article": "521304",
        "Family": "LE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17800"
    },
    {
        "Article": "521305",
        "Family": "LE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17800"
    },
    {
        "Article": "521306",
        "Family": "LE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17800"
    },
    {
        "Article": "521307",
        "Family": "LE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17800"
    },
    {
        "Article": "521302",
        "Family": "LE",
        "Cabinet size": "60C",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "17800"
    },
    {
        "Article": "526084",
        "Family": "LE",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "523334",
        "Family": "LE",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "523332",
        "Family": "LE",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "523335",
        "Family": "LE",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "523336",
        "Family": "LE",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "523337",
        "Family": "LE",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "16200"
    },
    {
        "Article": "526086",
        "Family": "LE",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "24800"
    },
    {
        "Article": "522209",
        "Family": "LE",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "24800"
    },
    {
        "Article": "522207",
        "Family": "LE",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "24800"
    },
    {
        "Article": "522210",
        "Family": "LE",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "24800"
    },
    {
        "Article": "522211",
        "Family": "LE",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "24800"
    },
    {
        "Article": "522212",
        "Family": "LE",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "24800"
    },
    {
        "Article": "526087",
        "Family": "LE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20900"
    },
    {
        "Article": "523328",
        "Family": "LE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20900"
    },
    {
        "Article": "523326",
        "Family": "LE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20900"
    },
    {
        "Article": "523329",
        "Family": "LE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20900"
    },
    {
        "Article": "523330",
        "Family": "LE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20900"
    },
    {
        "Article": "523331",
        "Family": "LE",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "20900"
    },
    {
        "Article": "526223",
        "Family": "LE",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "22400"
    },
    {
        "Article": "526226",
        "Family": "LE",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "22400"
    },
    {
        "Article": "526224",
        "Family": "LE",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "22400"
    },
    {
        "Article": "526227",
        "Family": "LE",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "22400"
    },
    {
        "Article": "526228",
        "Family": "LE",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "22400"
    },
    {
        "Article": "526229",
        "Family": "LE",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "22400"
    },
    {
        "Article": "526088",
        "Family": "LE",
        "Cabinet size": "80S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28900"
    },
    {
        "Article": "523165",
        "Family": "LE",
        "Cabinet size": "80S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28900"
    },
    {
        "Article": "523163",
        "Family": "LE",
        "Cabinet size": "80S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28900"
    },
    {
        "Article": "523166",
        "Family": "LE",
        "Cabinet size": "80S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28900"
    },
    {
        "Article": "523167",
        "Family": "LE",
        "Cabinet size": "80S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28900"
    },
    {
        "Article": "523168",
        "Family": "LE",
        "Cabinet size": "80S",
        "Installation type": "inset",
        "Material": "SILGRANIT",
        "Price": "28900"
    },
    {
        "Article": "519059",
        "Family": "LT",
        "Cabinet size": "45S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "17800"
    },
    {
        "Article": "525115",
        "Family": "LM",
        "Cabinet size": "45M",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "12600"
    },
    {
        "Article": "514785",
        "Family": "LV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "14200"
    },
    {
        "Article": "514786",
        "Family": "LV",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "13400"
    },
    {
        "Article": "513306",
        "Family": "RS",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "8800"
    },
    {
        "Article": "514647",
        "Family": "RS",
        "Cabinet size": "45Z",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "10600"
    },
    {
        "Article": "513313",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "8000"
    },
    {
        "Article": "525319",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "6100"
    },
    {
        "Article": "513314",
        "Family": "RV",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "8200"
    },
    {
        "Article": "525320",
        "Family": "TI",
        "Cabinet size": "45Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "6550"
    },
    {
        "Article": "516524",
        "Family": "TI",
        "Cabinet size": "45M",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "7100"
    },
    {
        "Article": "516525",
        "Family": "TI",
        "Cabinet size": "45M",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "8900"
    },
    {
        "Article": "511942",
        "Family": "TI",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "10900"
    },
    {
        "Article": "513441",
        "Family": "TI",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "7400"
    },
    {
        "Article": "513442",
        "Family": "TI",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "13100"
    },
    {
        "Article": "513675",
        "Family": "TI",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "9500"
    },
    {
        "Article": "511917",
        "Family": "FL",
        "Cabinet size": "45S",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "7900"
    },
    {
        "Article": "511918",
        "Family": "FL",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "6000"
    },
    {
        "Article": "512032",
        "Family": "FL",
        "Cabinet size": "45C",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "7800"
    },
    {
        "Article": "523001",
        "Family": "AD",
        "Cabinet size": "60C",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "66500"
    },
    {
        "Article": "523002",
        "Family": "AD",
        "Cabinet size": "60C",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "66500"
    },
    {
        "Article": "522104",
        "Family": "AS",
        "Cabinet size": "60S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "70200"
    },
    {
        "Article": "522105",
        "Family": "AS",
        "Cabinet size": "60S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "70200"
    },
    {
        "Article": "523140",
        "Family": "LT",
        "Cabinet size": "60C",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "20400"
    },
    {
        "Article": "525108",
        "Family": "LM",
        "Cabinet size": "60Z",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "14700"
    },
    {
        "Article": "525323",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "8900"
    },
    {
        "Article": "514796",
        "Family": "LT",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "25400"
    },
    {
        "Article": "525111",
        "Family": "LM",
        "Cabinet size": "60C",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "19600"
    },
    {
        "Article": "511908",
        "Family": "TI",
        "Cabinet size": "60X",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "20800"
    },
    {
        "Article": "511949",
        "Family": "TI",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "16500"
    },
    {
        "Article": "514813",
        "Family": "TB",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "10200"
    },
    {
        "Article": "511929",
        "Family": "TI",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "16900"
    },
    {
        "Article": "512303",
        "Family": "TB",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "10400"
    },
    {
        "Article": "513459",
        "Family": "TI",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "14300"
    },
    {
        "Article": "511926",
        "Family": "TI",
        "Cabinet size": "90Z",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "28900"
    },
    {
        "Article": "523667",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "69500"
    },
    {
        "Article": "516277",
        "Family": "LT",
        "Cabinet size": "90K",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "29900"
    },
    {
        "Article": "511582",
        "Family": "TI",
        "Cabinet size": "90K",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "10500"
    },
    {
        "Article": "524731",
        "Family": "PA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "39900"
    },
    {
        "Article": "524732",
        "Family": "PA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "39900"
    },
    {
        "Article": "524737",
        "Family": "PA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "39900"
    },
    {
        "Article": "524738",
        "Family": "PA",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "39900"
    },
    {
        "Article": "514486",
        "Family": "PX",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "45900"
    },
    {
        "Article": "514501",
        "Family": "PX",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "45900"
    },
    {
        "Article": "525156",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "69800"
    },
    {
        "Article": "525157",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "69800"
    },
    {
        "Article": "525161",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "69800"
    },
    {
        "Article": "525162",
        "Family": "ET",
        "Cabinet size": "60Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "69800"
    },
    {
        "Article": "525149",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "61000"
    },
    {
        "Article": "525150",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "61000"
    },
    {
        "Article": "525154",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "61000"
    },
    {
        "Article": "525155",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "61000"
    },
    {
        "Article": "525163",
        "Family": "VI",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "54800"
    },
    {
        "Article": "525164",
        "Family": "VI",
        "Cabinet size": "80Z",
        "Installation type": "inset",
        "Material": "керамика",
        "Price": "57900"
    },
    {
        "Article": "523726",
        "Family": "SU",
        "Cabinet size": "37E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "33600"
    },
    {
        "Article": "523727",
        "Family": "SU",
        "Cabinet size": "37E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "33600"
    },
    {
        "Article": "523731",
        "Family": "SU",
        "Cabinet size": "37E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "33600"
    },
    {
        "Article": "523732",
        "Family": "SU",
        "Cabinet size": "37E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "33600"
    },
    {
        "Article": "523733",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "34900"
    },
    {
        "Article": "523734",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "34900"
    },
    {
        "Article": "523739",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "34900"
    },
    {
        "Article": "523740",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "34900"
    },
    {
        "Article": "523741",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "38900"
    },
    {
        "Article": "523742",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "38900"
    },
    {
        "Article": "523746",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "38900"
    },
    {
        "Article": "523747",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "38900"
    },
    {
        "Article": "516035",
        "Family": "SU",
        "Cabinet size": "37E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "31200"
    },
    {
        "Article": "514506",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "32500"
    },
    {
        "Article": "514522",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "керамика",
        "Price": "36200"
    },
    {
        "Article": "525855",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "523483",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "523484",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "523485",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "523486",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "523487",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "523488",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "525854",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "523489",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "523490",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "523491",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "523492",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "523493",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "523494",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "525853",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "524663",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "524664",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "524665",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "524666",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "524667",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "524668",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "84500"
    },
    {
        "Article": "524669",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "525852",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "524670",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "524671",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "524672",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "524673",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "524674",
        "Family": "AX",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "72500"
    },
    {
        "Article": "523520",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "76500"
    },
    {
        "Article": "523521",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "76600"
    },
    {
        "Article": "523522",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "76600"
    },
    {
        "Article": "523523",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "76600"
    },
    {
        "Article": "523524",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "76600"
    },
    {
        "Article": "523525",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "76600"
    },
    {
        "Article": "523526",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "64800"
    },
    {
        "Article": "523527",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "64800"
    },
    {
        "Article": "523528",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "64800"
    },
    {
        "Article": "523529",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "64800"
    },
    {
        "Article": "523530",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "64800"
    },
    {
        "Article": "523531",
        "Family": "AX",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "64800"
    },
    {
        "Article": "525868",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "80800"
    },
    {
        "Article": "523670",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "80800"
    },
    {
        "Article": "523671",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "80800"
    },
    {
        "Article": "523672",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "80800"
    },
    {
        "Article": "523673",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "80800"
    },
    {
        "Article": "523674",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "80800"
    },
    {
        "Article": "523675",
        "Family": "DE",
        "Cabinet size": "90K",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "80800"
    },
    {
        "Article": "523789",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523819",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523792",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523820",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523794",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523821",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523795",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523822",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523797",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523823",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523799",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523824",
        "Family": "ZE",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "44900"
    },
    {
        "Article": "523884",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523909",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523886",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523910",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523888",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523911",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523889",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523912",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523891",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523913",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523892",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "523915",
        "Family": "ZE",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56700"
    },
    {
        "Article": "525917",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "519082",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "519081",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "519083",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "519084",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "519088",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "519086",
        "Family": "MA",
        "Cabinet size": "45S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "525870",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "24900"
    },
    {
        "Article": "517658",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "24900"
    },
    {
        "Article": "517171",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "24900"
    },
    {
        "Article": "517169",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "24900"
    },
    {
        "Article": "517167",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "24900"
    },
    {
        "Article": "518847",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "24900"
    },
    {
        "Article": "517166",
        "Family": "DA",
        "Cabinet size": "45Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "24900"
    },
    {
        "Article": "525872",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30700"
    },
    {
        "Article": "518533",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30700"
    },
    {
        "Article": "518536",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30700"
    },
    {
        "Article": "518532",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30700"
    },
    {
        "Article": "518531",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30700"
    },
    {
        "Article": "518849",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30700"
    },
    {
        "Article": "518530",
        "Family": "DA",
        "Cabinet size": "50Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30700"
    },
    {
        "Article": "525875",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "39200"
    },
    {
        "Article": "517657",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "39200"
    },
    {
        "Article": "515095",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "39200"
    },
    {
        "Article": "514771",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "39200"
    },
    {
        "Article": "514770",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "39200"
    },
    {
        "Article": "518851",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "39200"
    },
    {
        "Article": "514773",
        "Family": "DA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "39200"
    },
    {
        "Article": "525929",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55500"
    },
    {
        "Article": "519114",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55500"
    },
    {
        "Article": "519113",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55500"
    },
    {
        "Article": "519115",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55500"
    },
    {
        "Article": "519116",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55500"
    },
    {
        "Article": "519120",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55500"
    },
    {
        "Article": "519118",
        "Family": "MA",
        "Cabinet size": "60S",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55500"
    },
    {
        "Article": "519135",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "34300"
    },
    {
        "Article": "519134",
        "Family": "MA",
        "Cabinet size": "60Z",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "34300"
    },
    {
        "Article": "525933",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56000"
    },
    {
        "Article": "519151",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56000"
    },
    {
        "Article": "519150",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56000"
    },
    {
        "Article": "516522",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56000"
    },
    {
        "Article": "516523",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56000"
    },
    {
        "Article": "519157",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56000"
    },
    {
        "Article": "518883",
        "Family": "MA",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "56000"
    },
    {
        "Article": "525884",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "35600"
    },
    {
        "Article": "524856",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "35600"
    },
    {
        "Article": "524854",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "35600"
    },
    {
        "Article": "524857",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "35600"
    },
    {
        "Article": "524858",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "35600"
    },
    {
        "Article": "524859",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "35600"
    },
    {
        "Article": "524855",
        "Family": "EO",
        "Cabinet size": "60X",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "35600"
    },
    {
        "Article": "525981",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523396",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523397",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523398",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523399",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523400",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523401",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523402",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523403",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "523405",
        "Family": "SU",
        "Cabinet size": "16E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18500"
    },
    {
        "Article": "525983",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523406",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523407",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523408",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523409",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523410",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523411",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523412",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523414",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "523415",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22200"
    },
    {
        "Article": "525990",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523422",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523423",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523424",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523425",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523426",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523427",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523428",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523429",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "523431",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "526340",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "26100"
    },
    {
        "Article": "525995",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523432",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523433",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523434",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523435",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523436",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523437",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523438",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523439",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "523441",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "23500"
    },
    {
        "Article": "524107",
        "Family": "SU",
        "Cabinet size": "50F",
        "Installation type": "inset/flushmount",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "524110",
        "Family": "SU",
        "Cabinet size": "50F",
        "Installation type": "inset/flushmount",
        "Material": "SILGRANIT",
        "Price": "69800"
    },
    {
        "Article": "524111",
        "Family": "SU",
        "Cabinet size": "50F",
        "Installation type": "inset/flushmount",
        "Material": "SILGRANIT",
        "Price": "87200"
    },
    {
        "Article": "524112",
        "Family": "SU",
        "Cabinet size": "50F",
        "Installation type": "inset/flushmount",
        "Material": "SILGRANIT",
        "Price": "87200"
    },
    {
        "Article": "525887",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522227",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522228",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522229",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522230",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522231",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522232",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522234",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "522236",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38500"
    },
    {
        "Article": "526001",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523442",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523443",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523444",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523445",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523446",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523447",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523448",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523449",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "523451",
        "Family": "SU",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "29000"
    },
    {
        "Article": "526004",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523538",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523539",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523540",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523541",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523542",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523543",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523544",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523545",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "523547",
        "Family": "SU",
        "Cabinet size": "70L",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "32200"
    },
    {
        "Article": "525985",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523548",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523549",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523550",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523551",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523552",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523553",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523554",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523556",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523557",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "525986",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523558",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523559",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523560",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523561",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523562",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523563",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523564",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523565",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "523567",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "35200"
    },
    {
        "Article": "525991",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523151",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523152",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523153",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523154",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523155",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523156",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523157",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523158",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "523160",
        "Family": "SU",
        "Cabinet size": "43D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38600"
    },
    {
        "Article": "525987",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523574",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523575",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523576",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523577",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523578",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523579",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523580",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523581",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "523583",
        "Family": "SU",
        "Cabinet size": "35D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "38900"
    },
    {
        "Article": "526005",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523141",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523142",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523143",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523144",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523145",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523146",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523147",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523148",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "523150",
        "Family": "SU",
        "Cabinet size": "80E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "31800"
    },
    {
        "Article": "525993",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523584",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523585",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523586",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523587",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523588",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523589",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523590",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523591",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "523593",
        "Family": "SU",
        "Cabinet size": "48D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "40900"
    },
    {
        "Article": "525891",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525167",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525168",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525169",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525170",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525171",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525172",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525174",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "525176",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "45000"
    },
    {
        "Article": "526099",
        "Family": "RT",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "521346",
        "Family": "RT",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "521344",
        "Family": "RT",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "521347",
        "Family": "RT",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "521348",
        "Family": "RT",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "521349",
        "Family": "RT",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "22800"
    },
    {
        "Article": "526097",
        "Family": "RT",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18000"
    },
    {
        "Article": "521334",
        "Family": "RT",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18000"
    },
    {
        "Article": "521332",
        "Family": "RT",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18000"
    },
    {
        "Article": "521335",
        "Family": "RT",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18000"
    },
    {
        "Article": "521336",
        "Family": "RT",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18000"
    },
    {
        "Article": "521337",
        "Family": "RT",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "18000"
    },
    {
        "Article": "526098",
        "Family": "RT",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "19200"
    },
    {
        "Article": "523076",
        "Family": "RT",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "19200"
    },
    {
        "Article": "523075",
        "Family": "RT",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "19200"
    },
    {
        "Article": "524249",
        "Family": "RT",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "19200"
    },
    {
        "Article": "521352",
        "Family": "RT",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "19200"
    },
    {
        "Article": "521353",
        "Family": "RT",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "19200"
    },
    {
        "Article": "526096",
        "Family": "RT",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "523078",
        "Family": "RT",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "523077",
        "Family": "RT",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "524251",
        "Family": "RT",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "521350",
        "Family": "RT",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "521351",
        "Family": "RT",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "SILGRANIT",
        "Price": "27400"
    },
    {
        "Article": "525982",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "29500"
    },
    {
        "Article": "523416",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "29500"
    },
    {
        "Article": "523417",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "29500"
    },
    {
        "Article": "523418",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "29500"
    },
    {
        "Article": "523419",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "29500"
    },
    {
        "Article": "523420",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "29500"
    },
    {
        "Article": "523421",
        "Family": "SU",
        "Cabinet size": "32E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "29500"
    },
    {
        "Article": "525984",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55800"
    },
    {
        "Article": "523568",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55800"
    },
    {
        "Article": "523569",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55800"
    },
    {
        "Article": "523570",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55800"
    },
    {
        "Article": "523571",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55800"
    },
    {
        "Article": "523572",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55800"
    },
    {
        "Article": "523573",
        "Family": "SU",
        "Cabinet size": "34D",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "55800"
    },
    {
        "Article": "525988",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30000"
    },
    {
        "Article": "523475",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30000"
    },
    {
        "Article": "523495",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30000"
    },
    {
        "Article": "523496",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30000"
    },
    {
        "Article": "523497",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30000"
    },
    {
        "Article": "523498",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30000"
    },
    {
        "Article": "523499",
        "Family": "SU",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "30000"
    },
    {
        "Article": "525994",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "31900"
    },
    {
        "Article": "523532",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "31900"
    },
    {
        "Article": "523533",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "31900"
    },
    {
        "Article": "523534",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "31900"
    },
    {
        "Article": "523535",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "31900"
    },
    {
        "Article": "523536",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "31900"
    },
    {
        "Article": "523537",
        "Family": "SU",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "SILGRANIT",
        "Price": "31900"
    },
    {
        "Article": "526350",
        "Family": "UB",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "19600"
    },
    {
        "Article": "526351",
        "Family": "UB",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "22200"
    },
    {
        "Article": "526353",
        "Family": "UB",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "25400"
    },
    {
        "Article": "526355",
        "Family": "UB",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "27700"
    },
    {
        "Article": "518197",
        "Family": "UB",
        "Cabinet size": "18E",
        "Installation type": "flushmount",
        "Material": "нержавеющая сталь",
        "Price": "14000"
    },
    {
        "Article": "518199",
        "Family": "UB",
        "Cabinet size": "34E",
        "Installation type": "flushmount",
        "Material": "нержавеющая сталь",
        "Price": "15600"
    },
    {
        "Article": "518201",
        "Family": "UB",
        "Cabinet size": "40E",
        "Installation type": "flushmount",
        "Material": "нержавеющая сталь",
        "Price": "15900"
    },
    {
        "Article": "518203",
        "Family": "UB",
        "Cabinet size": "45E",
        "Installation type": "flushmount",
        "Material": "нержавеющая сталь",
        "Price": "17300"
    },
    {
        "Article": "518205",
        "Family": "UB",
        "Cabinet size": "50E",
        "Installation type": "flushmount",
        "Material": "нержавеющая сталь",
        "Price": "18500"
    },
    {
        "Article": "526113",
        "Family": "SJ",
        "Cabinet size": "18E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "15000"
    },
    {
        "Article": "526115",
        "Family": "SJ",
        "Cabinet size": "34E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "17000"
    },
    {
        "Article": "526117",
        "Family": "SJ",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "17800"
    },
    {
        "Article": "526120",
        "Family": "SJ",
        "Cabinet size": "45E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "20400"
    },
    {
        "Article": "526122",
        "Family": "SJ",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "21000"
    },
    {
        "Article": "526125",
        "Family": "SJ",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "25000"
    },
    {
        "Article": "526128",
        "Family": "SJ",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "33800"
    },
    {
        "Article": "526129",
        "Family": "SJ",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "33800"
    },
    {
        "Article": "526114",
        "Family": "SJ",
        "Cabinet size": "18E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "18000"
    },
    {
        "Article": "526116",
        "Family": "SJ",
        "Cabinet size": "34E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "21800"
    },
    {
        "Article": "526118",
        "Family": "SJ",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "22600"
    },
    {
        "Article": "526121",
        "Family": "SJ",
        "Cabinet size": "45E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "24600"
    },
    {
        "Article": "526123",
        "Family": "SJ",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "25000"
    },
    {
        "Article": "526126",
        "Family": "SJ",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "30000"
    },
    {
        "Article": "526130",
        "Family": "SJ",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "39900"
    },
    {
        "Article": "526131",
        "Family": "SJ",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "39900"
    },
    {
        "Article": "526119",
        "Family": "SJ",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "28200"
    },
    {
        "Article": "526124",
        "Family": "SJ",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "31300"
    },
    {
        "Article": "526127",
        "Family": "SJ",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "36800"
    },
    {
        "Article": "526132",
        "Family": "SJ",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "50000"
    },
    {
        "Article": "522952",
        "Family": "AN",
        "Cabinet size": "18E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "20700"
    },
    {
        "Article": "522955",
        "Family": "AN",
        "Cabinet size": "34E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "23300"
    },
    {
        "Article": "522959",
        "Family": "AN",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "23200"
    },
    {
        "Article": "522963",
        "Family": "AN",
        "Cabinet size": "45E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "26500"
    },
    {
        "Article": "522967",
        "Family": "AN",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "27700"
    },
    {
        "Article": "522971",
        "Family": "AN",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "35800"
    },
    {
        "Article": "522979",
        "Family": "AN",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "45400"
    },
    {
        "Article": "522977",
        "Family": "AN",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "45400"
    },
    {
        "Article": "522983",
        "Family": "AN",
        "Cabinet size": "33D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "48600"
    },
    {
        "Article": "522987",
        "Family": "AN",
        "Cabinet size": "40D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "55200"
    },
    {
        "Article": "522989",
        "Family": "AN",
        "Cabinet size": "50D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "48200"
    },
    {
        "Article": "522991",
        "Family": "AN",
        "Cabinet size": "50D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "48200"
    },
    {
        "Article": "522951",
        "Family": "AN",
        "Cabinet size": "18E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "24000"
    },
    {
        "Article": "522953",
        "Family": "AN",
        "Cabinet size": "34E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "27000"
    },
    {
        "Article": "522957",
        "Family": "AN",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "26600"
    },
    {
        "Article": "522961",
        "Family": "AN",
        "Cabinet size": "45E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "30800"
    },
    {
        "Article": "522965",
        "Family": "AN",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "32200"
    },
    {
        "Article": "522969",
        "Family": "AN",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "41000"
    },
    {
        "Article": "522975",
        "Family": "AN",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "52800"
    },
    {
        "Article": "522973",
        "Family": "AN",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "52800"
    },
    {
        "Article": "522981",
        "Family": "AN",
        "Cabinet size": "33D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "58200"
    },
    {
        "Article": "522985",
        "Family": "AN",
        "Cabinet size": "40D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "63800"
    },
    {
        "Article": "525244",
        "Family": "AN",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "42800"
    },
    {
        "Article": "525245",
        "Family": "AN",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "47000"
    },
    {
        "Article": "525246",
        "Family": "AN",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "49900"
    },
    {
        "Article": "525247",
        "Family": "AN",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "66800"
    },
    {
        "Article": "525248",
        "Family": "AN",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "71900"
    },
    {
        "Article": "525249",
        "Family": "AN",
        "Cabinet size": "40D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "78500"
    },
    {
        "Article": "521841",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "40000"
    },
    {
        "Article": "521840",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "45600"
    },
    {
        "Article": "521748",
        "Family": "ET",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "57800"
    },
    {
        "Article": "524270",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "47800"
    },
    {
        "Article": "524272",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "51600"
    },
    {
        "Article": "524274",
        "Family": "ET",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "59200"
    },
    {
        "Article": "523381",
        "Family": "CRKON",
        "Cabinet size": "80Z",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "158000"
    },
    {
        "Article": "525025",
        "Family": "CRKON",
        "Cabinet size": "60Z",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "152000"
    },
    {
        "Article": "521637",
        "Family": "FW",
        "Cabinet size": "50S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "234900"
    },
    {
        "Article": "521640",
        "Family": "FW",
        "Cabinet size": "60S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "160900"
    },
    {
        "Article": "521666",
        "Family": "JA",
        "Cabinet size": "60S",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "127500"
    },
    {
        "Article": "521556",
        "Family": "ZX",
        "Cabinet size": "34E",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "56900"
    },
    {
        "Article": "521558",
        "Family": "ZX",
        "Cabinet size": "40E",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "59100"
    },
    {
        "Article": "521559",
        "Family": "ZX",
        "Cabinet size": "50E",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "63200"
    },
    {
        "Article": "521560",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "inset",
        "Material": "нержавеющая сталь",
        "Price": "68100"
    },
    {
        "Article": "523096",
        "Family": "ZX",
        "Cabinet size": "34E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "62300"
    },
    {
        "Article": "523097",
        "Family": "ZX",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "67100"
    },
    {
        "Article": "523098",
        "Family": "ZX",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "70300"
    },
    {
        "Article": "523099",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "75300"
    },
    {
        "Article": "523100",
        "Family": "ZX",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "69700"
    },
    {
        "Article": "523101",
        "Family": "ZX",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "75500"
    },
    {
        "Article": "523102",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "77100"
    },
    {
        "Article": "521638",
        "Family": "ZX",
        "Cabinet size": "55E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "64500"
    },
    {
        "Article": "521631",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "64900"
    },
    {
        "Article": "521642",
        "Family": "ZX",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "110600"
    },
    {
        "Article": "521648",
        "Family": "ZX",
        "Cabinet size": "40D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "121500"
    },
    {
        "Article": "521566",
        "Family": "ZX",
        "Cabinet size": "18E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "53000"
    },
    {
        "Article": "521582",
        "Family": "ZX",
        "Cabinet size": "34E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "53800"
    },
    {
        "Article": "521584",
        "Family": "ZX",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "57900"
    },
    {
        "Article": "521586",
        "Family": "ZX",
        "Cabinet size": "45E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "59000"
    },
    {
        "Article": "521588",
        "Family": "ZX",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "60500"
    },
    {
        "Article": "521590",
        "Family": "ZX",
        "Cabinet size": "55E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "63000"
    },
    {
        "Article": "521592",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "64900"
    },
    {
        "Article": "521612",
        "Family": "ZX",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "108300"
    },
    {
        "Article": "521611",
        "Family": "ZX",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "108300"
    },
    {
        "Article": "521619",
        "Family": "ZX",
        "Cabinet size": "40D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "119500"
    },
    {
        "Article": "521567",
        "Family": "ZX",
        "Cabinet size": "18E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "47700"
    },
    {
        "Article": "521583",
        "Family": "ZX",
        "Cabinet size": "34E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "48200"
    },
    {
        "Article": "521585",
        "Family": "ZX",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "52200"
    },
    {
        "Article": "521587",
        "Family": "ZX",
        "Cabinet size": "45E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "53100"
    },
    {
        "Article": "521589",
        "Family": "ZX",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "54100"
    },
    {
        "Article": "521591",
        "Family": "ZX",
        "Cabinet size": "55E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "56300"
    },
    {
        "Article": "521593",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "64000"
    },
    {
        "Article": "521614",
        "Family": "ZX",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "96900"
    },
    {
        "Article": "521613",
        "Family": "ZX",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "96900"
    },
    {
        "Article": "521620",
        "Family": "ZX",
        "Cabinet size": "40D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "103400"
    },
    {
        "Article": "526243",
        "Family": "ZX",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "75500"
    },
    {
        "Article": "526245",
        "Family": "ZX",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "84200"
    },
    {
        "Article": "526244",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "89400"
    },
    {
        "Article": "526246",
        "Family": "ZX",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "97700"
    },
    {
        "Article": "523384",
        "Family": "CL",
        "Cabinet size": "34E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "63000"
    },
    {
        "Article": "523385",
        "Family": "CL",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "68100"
    },
    {
        "Article": "523386",
        "Family": "CL",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "70900"
    },
    {
        "Article": "523387",
        "Family": "CL",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "82000"
    },
    {
        "Article": "523388",
        "Family": "CL",
        "Cabinet size": "34E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "70900"
    },
    {
        "Article": "523389",
        "Family": "CL",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "75900"
    },
    {
        "Article": "523390",
        "Family": "CL",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "79600"
    },
    {
        "Article": "523391",
        "Family": "CL",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "84000"
    },
    {
        "Article": "523392",
        "Family": "CL",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "80300"
    },
    {
        "Article": "523393",
        "Family": "CL",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "83800"
    },
    {
        "Article": "523394",
        "Family": "CL",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "86500"
    },
    {
        "Article": "521632",
        "Family": "CL",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "67800"
    },
    {
        "Article": "521633",
        "Family": "CL",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "70600"
    },
    {
        "Article": "521639",
        "Family": "CL",
        "Cabinet size": "55E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "73300"
    },
    {
        "Article": "521634",
        "Family": "CL",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "73700"
    },
    {
        "Article": "521647",
        "Family": "CL",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "127400"
    },
    {
        "Article": "521654",
        "Family": "CL",
        "Cabinet size": "40D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "139300"
    },
    {
        "Article": "521623",
        "Family": "CS",
        "Cabinet size": "40S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "104700"
    },
    {
        "Article": "521624",
        "Family": "CS",
        "Cabinet size": "40S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "104700"
    },
    {
        "Article": "521625",
        "Family": "CS",
        "Cabinet size": "50S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "112800"
    },
    {
        "Article": "521626",
        "Family": "CS",
        "Cabinet size": "50S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "112800"
    },
    {
        "Article": "521645",
        "Family": "CS",
        "Cabinet size": "60S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "148700"
    },
    {
        "Article": "521646",
        "Family": "CS",
        "Cabinet size": "60S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "148700"
    },
    {
        "Article": "521651",
        "Family": "CS",
        "Cabinet size": "80S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "164800"
    },
    {
        "Article": "521652",
        "Family": "CS",
        "Cabinet size": "80S",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "164800"
    },
    {
        "Article": "521564",
        "Family": "CL",
        "Cabinet size": "18E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "58800"
    },
    {
        "Article": "521570",
        "Family": "CL",
        "Cabinet size": "34E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "60100"
    },
    {
        "Article": "521572",
        "Family": "CL",
        "Cabinet size": "40E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "64500"
    },
    {
        "Article": "521574",
        "Family": "CL",
        "Cabinet size": "45E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "65700"
    },
    {
        "Article": "521576",
        "Family": "CL",
        "Cabinet size": "50E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "67500"
    },
    {
        "Article": "521578",
        "Family": "CL",
        "Cabinet size": "55E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "70100"
    },
    {
        "Article": "521580",
        "Family": "CL",
        "Cabinet size": "70E",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "71100"
    },
    {
        "Article": "521608",
        "Family": "CL",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "131500"
    },
    {
        "Article": "521607",
        "Family": "CL",
        "Cabinet size": "34D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "131500"
    },
    {
        "Article": "521617",
        "Family": "CL",
        "Cabinet size": "40D",
        "Installation type": "inset/flushmount",
        "Material": "нержавеющая сталь",
        "Price": "145500"
    },
    {
        "Article": "521565",
        "Family": "CL",
        "Cabinet size": "18E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "52900"
    },
    {
        "Article": "521571",
        "Family": "CL",
        "Cabinet size": "34E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "54000"
    },
    {
        "Article": "521573",
        "Family": "CL",
        "Cabinet size": "40E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "57800"
    },
    {
        "Article": "521575",
        "Family": "CL",
        "Cabinet size": "45E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "59100"
    },
    {
        "Article": "521577",
        "Family": "CL",
        "Cabinet size": "50E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "60400"
    },
    {
        "Article": "521579",
        "Family": "CL",
        "Cabinet size": "55E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "62800"
    },
    {
        "Article": "521581",
        "Family": "CL",
        "Cabinet size": "70E",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "69700"
    },
    {
        "Article": "521610",
        "Family": "CL",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "117300"
    },
    {
        "Article": "521609",
        "Family": "CL",
        "Cabinet size": "34D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "117300"
    },
    {
        "Article": "521618",
        "Family": "CL",
        "Cabinet size": "40D",
        "Installation type": "undermount",
        "Material": "нержавеющая сталь",
        "Price": "125700"
    }
];
