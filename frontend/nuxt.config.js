const isProduction = process.env.NODE_ENV === 'production';

export default {
    ssr: true,
    target: 'server',

    head: {
        title: 'BLANCO UNIT',
        titleTemplate: '%s | BLANCO UNIT',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, height=device-height, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1' },
            { name: 'format-detection', content: 'telephone=no' },
            { name: 'theme-color', content: '#131313' },
        ],
        link: [
            { rel: 'shortcut icon', type: 'image/png', href: '/img/favicon.png' },
        ],
    },

    loading: {
        color: '#333399',
        failedColor: '#e33b3b',
        height: '3px',
    },

    styleResources: {
        scss: [
            '~/assets/scss/_variables.scss',
            '~/assets/scss/_mixins.scss',
        ],
    },

    css: [
        '@/assets/scss/main.scss',
    ],

    plugins: [
        { src: '@/plugins/vue-awesome-swiper', mode: 'client' },
        { src: '@/plugins/vuelidate', mode: 'client' },
        { src: '@/plugins/v-mask' },
        { src: '@/plugins/smooth-scrollbar', mode: 'client' },
        { src: '@/plugins/third-party-services.js', mode: 'client' },
    ],

    components: true,

    buildModules: isProduction ? [] : ['@nuxtjs/eslint-module', '@nuxtjs/stylelint-module'],

    modules: [
        '@nuxtjs/markdownit',
        '@nuxtjs/style-resources',
        '@nuxtjs/axios',
    ],

    axios: {
        baseURL: process.env.STRAPI_URL || 'http://blanco-unit-api.1gb.ru',
    },

    markdownit: {
        runtime: true,
    },

    build: {
        terser: {
            parallel: !isProduction, // настройка для хостинга, иначе сборка падает
        },
        optimization: {
            runtimeChunk: 'single',
            splitChunks: {
                chunks: 'all',
                maxInitialRequests: Infinity,
                minSize: 0,
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name(module) {
                            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
                            return `npm.${packageName.replace('@', '')}`;
                        },
                    },
                },
            },
        },
        // filenames: {
        //     app: ({ isDev }) => isDev ? '[name].js' : '[name].[contenthash].js', // [chunkhash].js
        //     chunk: ({ isDev }) => isDev ? '[name].js' : '[name].[contenthash].js', // [chunkhash].js
        //     css: ({ isDev }) => isDev ? '[name].css' : '[name].[contenthash].css', // [contenthash].css
        //     // img: ({ isDev }) => isDev ? '[path][name].[ext]' : 'img/[name].[ext]', // img/[hash:7].[ext]
        //     // font: ({ isDev }) => isDev ? '[path][name].[ext]' : 'fonts/[name].[contenthash].[ext]', // fonts/[hash:7].[ext]
        //     // video: ({ isDev }) => isDev ? '[path][name].[ext]' : 'videos/[name].[ext]' // videos/[hash:7].[ext]
        // },
    },

    router: {
        linkActiveClass: 'is-active',
        linkExactActiveClass: 'is-active-exact',
        prefetchLinks: false,
    },
};
