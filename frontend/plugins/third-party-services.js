import Vue from 'vue';

const asyncLoad = (src, callback) => {
    const script = document.createElement('script');
    const scriptFirst = document.getElementsByTagName('script')[0];
    script.src = src;
    script.defer = true;
    if (callback) { script.onload = callback; }
    scriptFirst.parentNode.insertBefore(script, scriptFirst);
};

export default ({ app }) => {
    if (process.env.NODE_ENV !== 'production') {
        return; // запускаем скрипты только на продакшене
    }

    let isLoaded = false;

    // window не используется из-за особенностей верстки
    document.querySelector('.layout').addEventListener('scroll', () => {
        if (!isLoaded) {
            isLoaded = true;

            // добавляем метрику на весь сайт
            asyncLoad('//mc.yandex.ru/metrika/tag.js', () => {
                const metrika = new window.Ya.Metrika2({
                    id: '83598076',
                    // clickmap: true,
                    // trackLinks: true,
                    // accurateTrackBounce: true,
                    webvisor: false,
                });

                app.router.afterEach((to, from) => {
                    // следим за переходами между страницами
                    metrika.hit(to.path, {
                        referer: from.path,
                    });
                });

                Vue.prototype.$metrika = metrika;
            });
        }
    }, false);
};
