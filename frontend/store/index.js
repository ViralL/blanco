export const strict = process.env.NODE_ENV !== 'production';

export const state = () => ({
    windowWidth: 320,
    showOrderButton: false,
});

export const getters = {
    getOrderButtonStatus: state => {
        return state.showOrderButton;
    },
};

export const mutations = {
    setWindowWidth(state, payload) {
        state.windowWidth = payload;
    },
    setOrderButtonStatus(state, payload) {
        state.showOrderButton = payload;
    },
};

export const actions = {};
