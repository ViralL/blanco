export const state = () => ({
    products: {
        mixer: {},
        washing: {},
        elemOne: {},
        elemTwo: {},
    },
});
export const getters = {
    getMixer: state => {
        return state.products.mixer;
    },
    getWashing: state => {
        return state.products.washing;
    },
};

export const mutations = {
    setProductsByType(state, payload) {
        state.products[payload.model] = payload.result;
    },
};

export const actions = {};
