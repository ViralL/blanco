export const state = () => ({
    compareItems: [],
});
export const getters = {
    getCompare: state => {
        return state.compareItems;
    },
};

export const mutations = {
    setCompareItems(state, payload) {
        state.compareItems.push(payload.result);
    },
    removeCompareItem(state, payload) {
        state.compareItems.splice(payload, 1);
    },
};

export const actions = {};
