export const state = () => ({
    cartItemsObj: [
        {
            compare: false,
            washing: null,
            mixer: null,
            garbage: null,
            accessories: null,
            filters: {
                indexes: {
                    washing: null,
                    mixer: null,
                    garbage: null,
                    accessories: null,
                },
                colorIndexes: {
                    washing: null,
                    mixer: null,
                    garbage: null,
                    accessories: null,
                },
                selected: {
                    washing: false,
                    mixer: false,
                    garbage: false,
                    accessories: false,
                },
                washing: {
                    price: 'none',
                    materialType: 'none',
                    sizeCupboard: 'none',
                    washingForm: 'none',
                    bowlType: 'none',
                    installationType: 'none',
                    hasDispenser: 'none',
                    hasAutomaticValve: 'none',
                    hasWing: 'none',
                },
                mixer: {
                    color: 'none',
                    heightType: 'none',
                    hasFilteredWater: 'none',
                    hasPulloutSpout: 'none',
                    hasFlexibleSpout: 'none',
                    hasRemovableHose: 'none',
                    hasShowerMode: 'none',
                    hasWindowInstallation: 'none',
                    hasTouchActivation: 'none',
                    hasWaterMeasurement: 'none',
                },
                garbage: {
                    facadeType: 'none',
                    containersCount: 'none',
                    depthBowl: 'none',
                },
                accessories: {
                    category: 'none',
                },
                kitchenStyle: 'classic',
                sizeCupboard: 60,
                typeCupboard: 'default',
            },
            canvasImg: {
                wall: null,
                tabletop: null,
                facade: null,
                washing: null,
                washingCoordinates: [0, 0],
                mixer: null,
                mixerCoordinates: [0, 0],
                garbage: null,
                accessories: null,
                washingMixerSide: null,
                wallFileName: null,
                tabletopFileName: null,
                facadeFileName: null,
                facadeType: null,
                facadeSize: null,
            },
        },
    ],
    activeItem: 0,
});
export const getters = {
    getCartObj: state => {
        return state.cartItemsObj;
    },
    getCartObjFilter: state => {
        return state.cartItemsObj[state.activeItem].filters;
    },
    getCartObjFilterSelected: state => {
        return state.cartItemsObj[state.activeItem].filters.selected;
    },
    getCartObjFilterIndex: state => {
        return state.cartItemsObj[state.activeItem].filters.indexes;
    },
    getCartObjFilterColorIndex: state => {
        return state.cartItemsObj[state.activeItem].filters.colorIndexes;
    },
    getCartObjCanvasImg: state => {
        return state.cartItemsObj[state.activeItem].canvasImg;
    },
    getActiveItem: state => {
        return state.activeItem;
    },
};

export const mutations = {
    setCartItemsByType(state, payload) {
        state.cartItemsObj[state.activeItem][payload.type] = payload.result;
        if (payload.num) {
            state.cartItemsObj[state.activeItem].filters.indexes[payload.type] = payload.num;
        }
    },
    setCartFilter(state, payload) {
        state.cartItemsObj[state.activeItem].filters = payload;
    },
    setCartCompare(state, payload) {
        state.cartItemsObj[payload.index].compare = payload.result;
    },
    setCartFilterByType(state, payload) {
        state.cartItemsObj[state.activeItem].filters[payload.type][payload.key] = payload.value;
    },
    setCartIndexFilter(state, payload) {
        state.cartItemsObj[state.activeItem].filters.indexes[payload.type] = payload.result;
    },
    setCartColorIndexFilter(state, payload) {
        state.cartItemsObj[state.activeItem].filters.colorIndexes[payload.type] = payload.result;
    },
    setCartSelectedFilter(state, payload) {
        state.cartItemsObj[state.activeItem].filters.selected[payload.type] = payload.result;
        if (!payload.result) {
            state.cartItemsObj[state.activeItem].filters[payload.type] = payload.initial;
        }
    },
    setCartCanvasImg(state, payload) {
        state.cartItemsObj[state.activeItem].canvasImg = payload;
    },
    setCartCanvasImgByItem(state, payload) {
        state.cartItemsObj[state.activeItem].canvasImg[payload.type] = payload.value;
        state.cartItemsObj[state.activeItem].canvasImg[payload.type + 'FileName'] = payload[payload.type + 'FileName'];
        if (payload.type === 'facade') {
            state.cartItemsObj[state.activeItem].canvasImg[payload.type + 'Type'] = payload[payload.type + 'Type'];
            state.cartItemsObj[state.activeItem].canvasImg[payload.type + 'Size'] = payload[payload.type + 'Size'];
        }
    },
    copyCartItemsByType(state, payload) {
        state.cartItemsObj.push(payload);
    },
    setActiveItem(state, payload) {
        state.activeItem = payload;
    },
    removeCartItem(state, payload) {
        state.cartItemsObj.splice(payload, 1);
    },
    removeCartItemIndexByObj(state, payload) {
        state.cartItemsObj[payload.index][payload.result] = null;
        // тут же удалим url для картинки удаляемого товара
        state.cartItemsObj[payload.index].canvasImg[payload.result] = null;
    },
    setSelectedStyleKitchen(state, payload) {
        state.cartItemsObj[state.activeItem].filters.kitchenStyle = payload;
    },
    setSelectedSizeCupboard(state, payload) {
        state.cartItemsObj[state.activeItem].filters.sizeCupboard = payload;
    },
    setSelectedTypeCupboard(state, payload) {
        state.cartItemsObj[state.activeItem].filters.typeCupboard = payload;
    },
};

export const actions = {};
