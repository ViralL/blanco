// https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#server
module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  // url: env('STRAPI_URL', ''),
  admin: {
    // url: env('STRAPI_ADMIN_URL', '/admin'),
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'f0b4395720725569a895b0e7f45cbc74'),
    },
  },
});
