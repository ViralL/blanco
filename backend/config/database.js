module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', 'mysql100.1gb.ru'),
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'gb_blancodb'),
        username: env('DATABASE_USERNAME', 'gb_blancodb'),
        password: env('DATABASE_PASSWORD', ''),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
