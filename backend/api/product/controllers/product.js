'use strict';

const _ = require('lodash');
const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const data = require('../data.json');
const data2 = require('../data2.json');
const data3 = require('../data3.json');
const data4 = require('../data4.json');
const data5 = require('../data5.json');

module.exports = {
  async loadFromFile() {
    // const categories = {
    //   'мойка': 1,
    //   'чаша': 2,
    //   'смеситель': 3,
    //   'мусорная система': 4,
    //   'дозатор': 5,
    //   'кнопка': 6,
    //   'фильтр': 7,
    // };

    // const productsInDB = (await strapi.services.product.find({ _limit: 2000 })).map(o => o.vendorCode);
    // const productsInFile = data.map(o => {
    //   const result = Number(o['Артикул']);
    //   // if (Number.isNaN(result)) {
    //   //   console.log(111, o);
    //   // }
    //   // H851P00 undefined
    //   // H211P07 undefined
    //   // H261P02 undefined
    //   return result;
    // });
    // const kinds = (await strapi.services.kind.find({ _limit: 2000 })).map(o => ({ id: o.id, name: o.name.toLowerCase() }));

    // const diff = _.difference(productsInFile, productsInDB);

    // в цикле перебираем элементы и записываем в базу
    // data.forEach(async (item) => {
    //   // vendorCode / Number
    //   // tags / Relation with Tag
    //   // category / Relation with Category
    //   // kind / Relation with Kind
    //   // name / Text
    //   // description / Rich text
    //   // color / Relation with Color
    //   // price / Number
    //   // quantity / Number
    //   // designType / Enumeration
    //   // picture / Media
    //   // galleryPictures / Media
    //   // equipment / Rich text
    //   // dimensionPictures / Media
    //   // pdf / Media
    //   // recommendation / Relation with Product
    //   // modelPictures
    //   //   front / Media
    //   //   side / Media
    //   //   other / Media
    //   // ttcBowl
    //   //   materialType / Enumeration
    //   //   sizeCupboard / Relation with SizeCupboard
    //   //   washingForm / Enumeration
    //   //   bowlType / Enumeration
    //   //   installationType / Enumeration
    //   //   hasWing / Boolean
    //   //   hasDispenser / Boolean
    //   //   hasAutomaticValve / Boolean
    //   // ttcMixer
    //   //   heightType / Enumeration
    //   //   hasFilteredWater / Boolean
    //   //   hasPulloutSpout / Boolean
    //   //   hasFlexibleSpout / Boolean
    //   //   hasRemovableHose / Boolean
    //   //   hasShowerMode / Boolean
    //   //   hasWindowInstallation / Boolean
    //   //   hasTouchActivation / Boolean
    //   //   hasWaterMeasurement / Boolean
    //   // ttcGarbageSystem
    //   //   containersCount / Number
    //   //   depthBowl / Number
    //   //   facadeType / Enumeration / Retractable | Hinged

    //   if (diff.includes(Number(item['Артикул']))) {
    //     const newProduct = {};
    //     newProduct.vendorCode = Number(item['Артикул']);
    //     // tags / Relation with Tag
    //     newProduct.category = categories[item['Категория']];
    //     if (item['Семейство']) {
    //       newProduct.kind = kinds.find(o => o.name === item['Семейство'].toLowerCase().trim()).id;
    //     }
    //     newProduct.name = item['Модель'];
    //     // newProduct.description = '';
    //     // newProduct.color = item['цвет/поверхность смесителя']; // color / Relation with Color
    //     newProduct.price = Number(item['Розница 01.02.21']);
    //     newProduct.designType = item['современный дизайн'] === '1' ? 'Modern' : 'Classic';
    //     // picture / Media
    //     // galleryPictures / Media
    //     // newProduct.equipment = '';
    //     // dimensionPictures / Media
    //     // pdf / Media
    //     // recommendation / Relation with Product
    //     // "Подходящий акссессуар 1": "218313",
    //     // "Подходящий акссессуар 2": "217611",
    //     // "Подходящий акссессуар 3": "223297",
    //     // "Подходящий акссессуар 4": "517666"

    //     await strapi.services.product.create(newProduct);
    //   }
    // });

    // обновим цены
    // const productsInDB = await strapi.services.product.find({ _start: 0, _limit: 200 });
    // productsInDB.forEach(async (item) => {
    //   await strapi.services.product.update({ id: 1 }, { name: item.name.trim() });
    // });

    // // обновим цены
    // _.each(data.slice(1200, 1500), async (item) => {
    //   if (Number.isNaN(Number(item['Артикул']))) return;

    //   await strapi.services.product.update({
    //     vendorCode: Number(item['Артикул']),
    //   }, {
    //     price: item['Розница 01.02.21'] ? Number(item['Розница 01.02.21'].replace(',', '')) : 0,
    //   });
    // });

    // // обновим цены
    // const productsInDB = await strapi.services.product.find({ _limit: 2000 });
    // _.each(productsInDB, async (item) => {
    //   if (item.price === 0) {
    //     const dataItem = data.find(o => Number(o['Артикул']) === item.vendorCode);
    //     await strapi.services.product.update({
    //       vendorCode: item.vendorCode,
    //     }, {
    //       price: Number(dataItem['Розница 01.02.21'].replace(',', '')),
    //     });
    //   }
    // });

    // // добавим компонент мусорной системы
    // const productsInDB = await strapi.services.product.find({ _limit: 2000, _where: { category: 4 } });
    // _.each(productsInDB, async (item) => {
    //   const dataItem = data.find(o => Number(o['Артикул']) === item.vendorCode);

    //   let containersCount = null;
    //   if (dataItem['1 контейнер']) {
    //     containersCount = 1;
    //   } else if (dataItem['Больше 1 контейнера']) {
    //     containersCount = 2;
    //   }

    //   let facadeType = null;
    //   if (dataItem['Выдвижной фасад']) {
    //     facadeType = 'Retractable';
    //   } else if (dataItem['Фасад на петлях']) {
    //     facadeType = 'Hinged';
    //   }

    //   const ttcGarbageSystem = {
    //     containersCount,
    //     depthBowl: 0,
    //     facadeType,
    //   };

    //   await strapi.services.product.update({
    //     vendorCode: item.vendorCode,
    //   }, {
    //     ttcGarbageSystem,
    //   });
    // });

    // // добавим компонент смесителя
    // const productsInDB = await strapi.services.product.find({ _limit: 2000, _where: { category: 3 } });
    // _.each(productsInDB.slice(150, 300), async (item) => {
    //   const dataItem = data.find(o => Number(o['Артикул']) === item.vendorCode);

    //   let heightType = null;
    //   if (dataItem['Низкий излив']) {
    //     heightType = 'LowSpout';
    //   } else if (dataItem['Высокий излив']) {
    //     heightType = 'HighSpout';
    //   }

    //   let hasFilteredWater = false;
    //   if (dataItem['Фильтрованная вода для смесителя']) {
    //     hasFilteredWater = true;
    //   }

    //   let hasPulloutSpout = false;
    //   if (dataItem['Выдвжиной излив']) {
    //     hasPulloutSpout = true;
    //   }

    //   let hasFlexibleSpout = false;
    //   if (dataItem['Гибкий излив']) {
    //     hasFlexibleSpout = true;
    //   }

    //   let hasRemovableHose = false;
    //   if (dataItem['Съемный шланг']) {
    //     hasRemovableHose = true;
    //   }

    //   let hasShowerMode = false;
    //   if (dataItem['Режим "душ"']) {
    //     hasShowerMode = true;
    //   }

    //   let hasWindowInstallation = false;
    //   if (dataItem['Установка у окна']) {
    //     hasWindowInstallation = true;
    //   }

    //   let hasTouchActivation = false;
    //   if (dataItem['Премиум-функция: сенсорное включение']) {
    //     hasTouchActivation = true;
    //   }

    //   let hasWaterMeasurement = false;
    //   if (dataItem['Премиум-функция: измерение объема воды']) {
    //     hasWaterMeasurement = true;
    //   }

    //   const ttcMixer = {
    //     heightType,
    //     hasFilteredWater,
    //     hasPulloutSpout,
    //     hasFlexibleSpout,
    //     hasRemovableHose,
    //     hasShowerMode,
    //     hasWindowInstallation,
    //     hasTouchActivation,
    //     hasWaterMeasurement,
    //   };

    //   await strapi.services.product.update({
    //     vendorCode: item.vendorCode,
    //   }, {
    //     ttcMixer,
    //   });
    // });

    // // добавим компонент мойки и чаши
    // const productsInDB = await strapi.services.product.find({ _limit: 2000, _where: { category: 2 } }); // category: 1
    // for (const item of productsInDB.slice(400, 600)) {
    //   const dataItem = data.find(o => Number(o['Артикул']) === item.vendorCode);

    //   let materialType = null;
    //   if (dataItem['Гранит']) {
    //     materialType = 'Granite';
    //   } else if (dataItem['Нержавеющая сталь']) {
    //     materialType = 'StainlessSteel';
    //   } else if (dataItem['Керамика']) {
    //     materialType = 'Ceramics';
    //   }

    //   let sizeCupboard = null;
    //   if (dataItem['Размер шкафа 40  см']) {
    //     sizeCupboard = 1;
    //   } else if (dataItem['Размер шкафа 45 см']) {
    //     sizeCupboard = 2;
    //   } else if (dataItem['Размер шкафа 50 см']) {
    //     sizeCupboard = 3;
    //   } else if (dataItem['Размер шкафа 60 см']) {
    //     sizeCupboard = 4;
    //   } else if (dataItem['Размер шкафа 80 см']) {
    //     sizeCupboard = 5;
    //   } else if (dataItem['Размер шкафа 90 см']) {
    //     sizeCupboard = 6;
    //   } else if (dataItem['Угловой шкаф']) {
    //     sizeCupboard = 7;
    //   }

    //   let washingForm = null;
    //   if (dataItem['Круглая мойка']) {
    //     washingForm = 'Round';
    //   } else if (dataItem['Прямоугольная мойка']) {
    //     washingForm = 'Rectangular';
    //   }

    //   let bowlType = null;
    //   if (dataItem['Одна чаша']) {
    //     bowlType = 'One';
    //   } else if (dataItem['Две чаши']) {
    //     bowlType = 'Two';
    //   } else if (dataItem['Подходит для комбинации чаш']) {
    //     bowlType = 'Combine';
    //   }

    //   let installationType = 'Mortise';
    //   if (dataItem['Врезной монтаж']) {
    //     installationType = 'Mortise';
    //   } else if (dataItem['Подстольный монтаж']) {
    //     installationType = 'UnderTable';
    //   } else if (dataItem['Монтаж в один уровень']) {
    //     installationType = 'OneLevel';
    //   } else if (dataItem['С видимой фронтальной стороной']) {
    //     installationType = 'WithVisibleFrontSide';
    //   }

    //   let hasWing = false;
    //   if (dataItem['Крыло']) {
    //     hasWing = true;
    //   }

    //   let hasDispenser = false;

    //   let hasAutomaticValve = false;

    //   const ttcBowl = {
    //     materialType,
    //     sizeCupboard,
    //     washingForm,
    //     bowlType,
    //     installationType,
    //     hasWing,
    //     hasDispenser,
    //     hasAutomaticValve,
    //   };

    //   await strapi.services.product.update({
    //     vendorCode: item.vendorCode,
    //   }, {
    //     ttcBowl,
    //   });
    // }

    // // обновим цвет у продуктов
    // const colorsInDB = await strapi.services.color.find({ _limit: 100 });
    // const productsInDB = await strapi.services.product.find({ _limit: 2000 });
    // for (const [index, item] of productsInDB.slice(1400, 1500).entries()) {
    //   const dataItem = data.find(o => Number(o['Артикул']) === item.vendorCode);

    //   const value = dataItem['цвет/поверхность смесителя'];

    //   let color = null;

    //   if (value === 'Нержавещая сталь' || value === 'Нержавеющая сталь') {
    //     color = 13;
    //   } else if (value) {
    //     color = colorsInDB.find(o => o.name.toLowerCase() === value.toLowerCase()).id;
    //   }

    //   console.log(index, item.vendorCode, color);

    //   await strapi.services.product.update({
    //     vendorCode: item.vendorCode,
    //   }, {
    //     color,
    //   });
    // }

    // // добавим описание продуктам
    // const productsInDB = await strapi.services.product.find({ _limit: 2000 });
    // for (const [index, item] of productsInDB.entries()) {
    //   const dataItem = data2.find(o => Number(o['Item number']) === item.vendorCode);

    //   if (dataItem) {
    //     const value = dataItem['Text - Key slogan (Russian)'];
    //     let value2 = dataItem['Text - Bullet points (Russian)'];

    //     if (value2) {
    //       value2 = value2.replace('<ul><li>', '\n- ').replace('</li></ul>', '');
    //       value2 = value2.split('</li><li>').join('\n- ');

    //       const description = value + '\n' + value2;

    //       console.log(index, item.vendorCode);

    //       await strapi.services.product.update({
    //         vendorCode: item.vendorCode,
    //       }, {
    //         description,
    //       });
    //     }
    //   }
    // }

    // // изменим размер шкафа
    // const sizeCupboardsInDB = await strapi.services['size-cupboard'].find({ _limit: 100 });
    // const productsInDB = await strapi.services.product.find({ _limit: 2000 });
    // for (const [index, item] of productsInDB.entries()) {
    //   const dataItem = data3.find(o => Number(o['Article']) === item.vendorCode);
    //   const dataItem2 = data.find(o => Number(o['Артикул']) === item.vendorCode);

    //   if (dataItem && dataItem2) {
    //     const value = dataItem['Cabinet size'];

    //     try {
    //       let sizeCupboardId = sizeCupboardsInDB.find(o => o.size.toLowerCase() === value.toLowerCase()).id;

    //       console.log(index, item.vendorCode, sizeCupboardId, value);

    //       let materialType = null;
    //       if (dataItem2['Гранит']) {
    //         materialType = 'Granite';
    //       } else if (dataItem2['Нержавеющая сталь']) {
    //         materialType = 'StainlessSteel';
    //       } else if (dataItem2['Керамика']) {
    //         materialType = 'Ceramics';
    //       }

    //       let washingForm = null;
    //       if (dataItem2['Круглая мойка']) {
    //         washingForm = 'Round';
    //       } else if (dataItem2['Прямоугольная мойка']) {
    //         washingForm = 'Rectangular';
    //       }

    //       let bowlType = null;
    //       if (dataItem2['Одна чаша']) {
    //         bowlType = 'One';
    //       } else if (dataItem2['Две чаши']) {
    //         bowlType = 'Two';
    //       } else if (dataItem2['Подходит для комбинации чаш']) {
    //         bowlType = 'Combine';
    //       }

    //       let installationType = 'Mortise';
    //       if (dataItem2['Врезной монтаж']) {
    //         installationType = 'Mortise';
    //       } else if (dataItem2['Подстольный монтаж']) {
    //         installationType = 'UnderTable';
    //       } else if (dataItem2['Монтаж в один уровень']) {
    //         installationType = 'OneLevel';
    //       } else if (dataItem2['С видимой фронтальной стороной']) {
    //         installationType = 'WithVisibleFrontSide';
    //       }

    //       let hasWing = false;
    //       if (dataItem2['Крыло']) {
    //         hasWing = true;
    //       }

    //       const hasDispenser = false;

    //       const hasAutomaticValve = false;

    //       const mixerSide = 'Front';

    //       const ttcBowl = {
    //         id: item.ttcBowl.id, // чтобы обновлялся существующий компонент, а не пересоздавался текущий
    //         materialType,
    //         sizeCupboard: sizeCupboardId,
    //         washingForm,
    //         bowlType,
    //         installationType,
    //         hasWing,
    //         hasDispenser,
    //         hasAutomaticValve,
    //         mixerSide,
    //       };

    //       await strapi.services.product.update({
    //         vendorCode: item.vendorCode,
    //       }, {
    //         ttcBowl,
    //       });

    //     } catch (e) {
    //       // f
    //     }
    //   }
    // }

    // // обновим тип монтажа
    // const productsInDB = await strapi.services.product.find({ _limit: 2000 });
    // for (const [index, item] of productsInDB.entries()) {
    //   const dataItem = data3.find(o => Number(o['Article']) === item.vendorCode);

    //   if (dataItem) {
    //     const value = dataItem['Installation type'].replace('/', ''); // from inset/flushmount to insetflushmount

    //     try {
    //       console.log(index, item.vendorCode, value);

    //       const ttcBowl = {
    //         id: item.ttcBowl.id, // чтобы обновлялся существующий компонент, а не пересоздавался текущий
    //         installationType: value,
    //       };

    //       await strapi.services.product.update({
    //         vendorCode: item.vendorCode,
    //       }, {
    //         ttcBowl,
    //       });
    //     } catch (e) {
    //       // f
    //     }
    //   }
    // }

    // // проставим segment товаров
    // const productsInDB = await strapi.services.product.find({ _limit: 2000 });
    // for (const [index, item] of productsInDB.entries()) {
    //   const dataItem = data4.find(o => Number(o['Article']) === item.vendorCode);

    //   if (dataItem) {
    //     try {
    //       console.log(index, item.vendorCode, dataItem['Segment']);

    //       await strapi.services.product.update({
    //         vendorCode: item.vendorCode,
    //       }, {
    //         segment: dataItem['Segment'],
    //       });
    //     } catch (e) {
    //       // f
    //     }
    //   }
    // }

    // // проставим discountPercentage товаров
    // const productsInDB = await strapi.services.product.find({ _limit: -1 });
    // for (const [index, item] of productsInDB.entries()) {
    //   const dataItem = data5.find(o => o[0] === item.vendorCode);

    //   if (dataItem) {
    //     try {
    //       console.log(index, item.vendorCode, dataItem[1]);

    //       await strapi.services.product.update({
    //         vendorCode: item.vendorCode,
    //       }, {
    //         discountPercentage: dataItem[1],
    //       });
    //     } catch (e) {
    //       // f
    //     }
    //   }
    // }

    return 'OK';
  },

  async findExtra(ctx) {
    const { query } = ctx;

    let entities;
    if (query._q) {
      entities = await strapi.services.product.search(query);
    } else {
      // http://knexjs.org/#Builder
      // execute the query with a promise, callback, or stream
      const knex = strapi.connections.default;
      let ids = []; // потребуется если используется поиск по компонентам id_in: [];
      let hasQueryComponents = query.ttcBowl || query.ttcMixer || query.ttcGarbageSystem;

      // сначала достанем id товаров по компонентам
      if (query.ttcBowl) {
        const queryBuilder = knex('components_tactical_technical_characteristics_bowls')
          .join('size_cupboards', 'size_cupboards.id', 'components_tactical_technical_characteristics_bowls.sizeCupboard')
          .join('products_components', 'products_components.component_id', 'components_tactical_technical_characteristics_bowls.id')
          .where('products_components.field', 'ttcBowl');

        if (query.ttcBowl.designType) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_bowls.designType', query.ttcBowl.designType);
        }

        if (query.ttcBowl.materialType) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_bowls.materialType', query.ttcBowl.materialType);
        }

        if (query.ttcBowl.sizeCupboard && query.ttcBowl.sizeCupboard.size) {
          queryBuilder.whereIn('size_cupboards.size', query.ttcBowl.sizeCupboard.size);
        }

        if (query.ttcBowl.washingForm) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_bowls.washingForm', query.ttcBowl.washingForm);
        }

        if (query.ttcBowl.bowlType) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_bowls.bowlType', query.ttcBowl.bowlType);
        }

        if (query.ttcBowl.installationType) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_bowls.installationType', query.ttcBowl.installationType);
        }

        if (query.ttcBowl.mixerSide) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_bowls.mixerSide', query.ttcBowl.mixerSide);
        }

        if (query.ttcBowl.hasWing) {
          queryBuilder.where('components_tactical_technical_characteristics_bowls.hasWing', query.ttcBowl.hasWing);
        }

        if (query.ttcBowl.hasDispenser) {
          queryBuilder.where('components_tactical_technical_characteristics_bowls.hasDispenser', query.ttcBowl.hasDispenser);
        }

        if (query.ttcBowl.hasAutomaticValve) {
          queryBuilder.where('components_tactical_technical_characteristics_bowls.hasAutomaticValve', query.ttcBowl.hasAutomaticValve);
        }

        const result = await queryBuilder.select('products_components.product_id');

        ids = _.map(result, o => o.product_id);

        delete query.ttcBowl; // удалим чтобы ключ не попадал в общий запрос
      }

      if (query.ttcMixer) {
        const queryBuilder = knex('components_tactical_technical_characteristics_mixers')
          .join('products_components', 'products_components.component_id', 'components_tactical_technical_characteristics_mixers.id')
          .where('products_components.field', 'ttcMixer');

        if (query.ttcMixer.heightType) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_mixers.heightType', query.ttcMixer.heightType);
        }

        if (query.ttcMixer.hasFilteredWater) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasFilteredWater', query.ttcMixer.hasFilteredWater);
        }

        if (query.ttcMixer.hasPulloutSpout) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasPulloutSpout', query.ttcMixer.hasPulloutSpout);
        }

        if (query.ttcMixer.hasFlexibleSpout) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasFlexibleSpout', query.ttcMixer.hasFlexibleSpout);
        }

        if (query.ttcMixer.hasRemovableHose) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasRemovableHose', query.ttcMixer.hasRemovableHose);
        }

        if (query.ttcMixer.hasShowerMode) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasShowerMode', query.ttcMixer.hasShowerMode);
        }

        if (query.ttcMixer.hasWindowInstallation) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasWindowInstallation', query.ttcMixer.hasWindowInstallation);
        }

        if (query.ttcMixer.hasTouchActivation) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasTouchActivation', query.ttcMixer.hasTouchActivation);
        }

        if (query.ttcMixer.hasWaterMeasurement) {
          queryBuilder.where('components_tactical_technical_characteristics_mixers.hasWaterMeasurement', query.ttcMixer.hasWaterMeasurement);
        }

        const result = await queryBuilder.select('products_components.product_id');

        ids = _.map(result, o => o.product_id);

        delete query.ttcMixer; // удалим чтобы ключ не попадал в общий запрос
      }

      if (query.ttcGarbageSystem) {
        const queryBuilder = knex('components_tactical_technical_characteristics_garbage_systems')
          .join('products_components', 'products_components.component_id', 'components_tactical_technical_characteristics_garbage_systems.id')
          .where('products_components.field', 'ttcGarbageSystem');

        if (query.ttcGarbageSystem.containersCount) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_garbage_systems.containersCount', query.ttcGarbageSystem.containersCount);
        }

        if (query.ttcGarbageSystem.depthBowl) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_garbage_systems.depthBowl', query.ttcGarbageSystem.depthBowl);
        }

        if (query.ttcGarbageSystem.facadeType) {
          queryBuilder.whereIn('components_tactical_technical_characteristics_garbage_systems.facadeType', query.ttcGarbageSystem.facadeType);
        }

        const result = await queryBuilder.select('products_components.product_id');

        ids = _.map(result, o => o.product_id);

        delete query.ttcGarbageSystem; // удалим чтобы ключ не попадал в общий запрос
      }

      // если в компонентах не нашлось подходящих id, то бесмысленно искать продукты
      if (hasQueryComponents && ids.length === 0) {
        return [];
      }

      // добавим более точечную выборку по id товаров
      if (ids.length > 0) {
        query.id_in = ids;
      }

      entities = await strapi.services.product.find(query);
    }

    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.product }));
  },

  async findOneByVendorCode(context) {
    const { vendorCode } = context.params;

    return await strapi.services.product.findOne({ _where: { vendorCode } });
  },
};
