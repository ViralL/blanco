'use strict';

const fs = require('fs');
const XLSX = require('xlsx');
const XLSXPopulate = require('xlsx-populate');
const _ = require('lodash');

/**
 * import-prices.js controller
 *
 * @description: A set of functions called "actions" of the `import-prices` plugin.
 */

const hasValue = value => {
  return value !== undefined && value !== null;
};

module.exports = {
  async upload(context) {
    const { data } = context.request.body; // в data содержится контент из файла в binary виде

    try {
      // скрипт не создаёт новых записей, он изменяет только существующие
      const knex = strapi.connections.default;
      const productsVendorCodes = await knex('products').select('id', 'vendorCode');
      const productsComponents = await knex('products_components').select('component_id', 'product_id', 'field');

      // считываем входящий файл и парсим в json
      const workbook = XLSX.read(data, { type: 'binary' });
      // const sheetNames = workbook.SheetNames; // product | ttcBowl | ttcMixer | ttcGarbageSystem

      /////////////////////////// ПРОДУКТЫ ////////////////////////////////
      const productsFromXLSX = XLSX.utils.sheet_to_json(workbook.Sheets.product);
      let responseByProduct = [];

      // проверка что есть данные на вкладке product и что все колонки на месте
      if (productsFromXLSX.length > 0) {
        const validColumnsName = ['vendorCode', 'category', 'name', 'color', 'price', 'discountPercentage', 'designType', 'segment'];
        const columnsNameFromProduct = Object.keys(productsFromXLSX[0]);

        if (!_.isEqual(_.sortBy(validColumnsName), _.sortBy(columnsNameFromProduct))) {
          return {
            error: 'XLSX list `product` is not in the correct format',
          };
        }

        // массово обновляем данные
        responseByProduct = await knex.transaction(trx => {
          const queries = [];

          productsVendorCodes.forEach(item => {
            const xlsxRow = productsFromXLSX.find(o => o['vendorCode'] === item.vendorCode);
            if (xlsxRow) {
              const dataForUpdate = {};

              // vendorCode нельзя обновлять
              // if (xlsxRow['vendorCode']) {
              //   dataForUpdate.vendorCode = xlsxRow['vendorCode'];
              // }

              if (hasValue(xlsxRow['category'])) {
                dataForUpdate.category = Number(xlsxRow['category'].split(' ')[0]);
              }

              if (hasValue(xlsxRow['name'])) {
                dataForUpdate.name = xlsxRow['name'];
              }

              if (hasValue(xlsxRow['color']) && xlsxRow['color'] !== '—') {
                dataForUpdate.price = Number(xlsxRow['color'].split(' ')[0]);
              }

              if (hasValue(xlsxRow['price'])) {
                dataForUpdate.price = xlsxRow['price'];
              }

              if (hasValue(xlsxRow['discountPercentage'])) {
                dataForUpdate.discountPercentage = Number(xlsxRow['discountPercentage']);
              }

              if (hasValue(xlsxRow['designType'])) {
                dataForUpdate.designType = xlsxRow['designType'];
              }

              if (hasValue(xlsxRow['segment'])) {
                dataForUpdate.segment = xlsxRow['segment'];
              }

              // This makes every update be in the same transaction
              const query = knex('products').where('vendorCode', item.vendorCode).update(dataForUpdate).transacting(trx);
              queries.push(query);
            }
          });

          Promise.all(queries) // Once every query is written
            .then(trx.commit) // We try to execute all of them
            .catch(trx.rollback); // And rollback in case any of them goes wrong
        });
      }

      /////////////////////////// МОЙКИ ////////////////////////////////
      const ttcBowlFromXLSX = XLSX.utils.sheet_to_json(workbook.Sheets.ttcBowl);
      let responseByTtcBowl = [];

      // проверка что есть данные на вкладке ttcBowl и что все колонки на месте
      if (ttcBowlFromXLSX.length > 0) {
        const validColumnsName = ['vendorCode', 'materialType', 'sizeCupboard', 'washingForm', 'bowlType', 'installationType', 'hasWing', 'hasDispenser', 'hasAutomaticValve', 'mixerSide'];
        const columnsNameFromTtcBowlFromXLSX = Object.keys(ttcBowlFromXLSX[0]);

        if (!_.isEqual(_.sortBy(validColumnsName), _.sortBy(columnsNameFromTtcBowlFromXLSX))) {
          return {
            error: 'XLSX list `ttcBowl` is not in the correct format',
          };
        }

        // массово обновляем данные
        responseByTtcBowl = await knex.transaction(trx => {
          const queries = [];

          productsVendorCodes.forEach(item => {
            const xlsxRow = ttcBowlFromXLSX.find(o => o['vendorCode'] === item.vendorCode);
            if (xlsxRow) {
              const productComponent = productsComponents.find(o => o.product_id === item.id && o.field === 'ttcBowl');
              if (productComponent) {
                const dataForUpdate = {};

                if (hasValue(xlsxRow['materialType'])) {
                  dataForUpdate.materialType = xlsxRow['materialType'];
                }

                if (hasValue(xlsxRow['sizeCupboard'])) {
                  dataForUpdate.sizeCupboard = Number(xlsxRow['sizeCupboard'].split(' ')[0]);
                }

                if (hasValue(xlsxRow['washingForm'])) {
                  dataForUpdate.washingForm = xlsxRow['washingForm'];
                }

                if (hasValue(xlsxRow['bowlType'])) {
                  dataForUpdate.bowlType = xlsxRow['bowlType'];
                }

                if (hasValue(xlsxRow['installationType'])) {
                  dataForUpdate.installationType = xlsxRow['installationType'];
                }

                if (hasValue(xlsxRow['hasWing'])) {
                  dataForUpdate.hasWing = xlsxRow['hasWing'];
                }

                if (hasValue(xlsxRow['hasDispenser'])) {
                  dataForUpdate.hasDispenser = xlsxRow['hasDispenser'];
                }

                if (hasValue(xlsxRow['hasAutomaticValve'])) {
                  dataForUpdate.hasAutomaticValve = xlsxRow['hasAutomaticValve'];
                }

                if (hasValue(xlsxRow['mixerSide'])) {
                  dataForUpdate.mixerSide = xlsxRow['mixerSide'];
                }

                // This makes every update be in the same transaction
                const query = knex('components_tactical_technical_characteristics_bowls')
                  .where('id', productComponent.component_id).update(dataForUpdate).transacting(trx);
                queries.push(query);
              }
            }
          });

          Promise.all(queries) // Once every query is written
            .then(trx.commit) // We try to execute all of them
            .catch(trx.rollback); // And rollback in case any of them goes wrong
        });
      }

      /////////////////////////// СМЕСИТЕЛИ ////////////////////////////////
      const ttcMixerFromXLSX = XLSX.utils.sheet_to_json(workbook.Sheets.ttcMixer);
      let responseByTtcMixer = [];

      // проверка что есть данные на вкладке ttcMixer и что все колонки на месте
      if (ttcMixerFromXLSX.length > 0) {
        const validColumnsName = ['vendorCode', 'heightType', 'hasFilteredWater', 'hasPulloutSpout', 'hasFlexibleSpout', 'hasRemovableHose', 'hasShowerMode', 'hasWindowInstallation', 'hasTouchActivation', 'hasWaterMeasurement'];
        const columnsNameFromTtcMixerFromXLSX = Object.keys(ttcMixerFromXLSX[0]);

        if (!_.isEqual(_.sortBy(validColumnsName), _.sortBy(columnsNameFromTtcMixerFromXLSX))) {
          return {
            error: 'XLSX list `ttcMixer` is not in the correct format',
          };
        }

        // массово обновляем данные
        responseByTtcMixer = await knex.transaction(trx => {
          const queries = [];

          productsVendorCodes.forEach(item => {
            const xlsxRow = ttcMixerFromXLSX.find(o => o['vendorCode'] === item.vendorCode);
            if (xlsxRow) {
              const productComponent = productsComponents.find(o => o.product_id === item.id && o.field === 'ttcMixer');
              if (productComponent) {
                const dataForUpdate = {};

                if (hasValue(xlsxRow['heightType'])) {
                  dataForUpdate.heightType = xlsxRow['heightType'];
                }

                if (hasValue(xlsxRow['hasFilteredWater'])) {
                  dataForUpdate.hasFilteredWater = xlsxRow['hasFilteredWater'];
                }

                if (hasValue(xlsxRow['hasPulloutSpout'])) {
                  dataForUpdate.hasPulloutSpout = xlsxRow['hasPulloutSpout'];
                }

                if (hasValue(xlsxRow['hasFlexibleSpout'])) {
                  dataForUpdate.hasFlexibleSpout = xlsxRow['hasFlexibleSpout'];
                }

                if (hasValue(xlsxRow['hasRemovableHose'])) {
                  dataForUpdate.hasRemovableHose = xlsxRow['hasRemovableHose'];
                }

                if (hasValue(xlsxRow['hasShowerMode'])) {
                  dataForUpdate.hasShowerMode = xlsxRow['hasShowerMode'];
                }

                if (hasValue(xlsxRow['hasWindowInstallation'])) {
                  dataForUpdate.hasWindowInstallation = xlsxRow['hasWindowInstallation'];
                }

                if (hasValue(xlsxRow['hasTouchActivation'])) {
                  dataForUpdate.hasTouchActivation = xlsxRow['hasTouchActivation'];
                }

                if (hasValue(xlsxRow['hasWaterMeasurement'])) {
                  dataForUpdate.hasWaterMeasurement = xlsxRow['hasWaterMeasurement'];
                }

                // This makes every update be in the same transaction
                const query = knex('components_tactical_technical_characteristics_mixers')
                  .where('id', productComponent.component_id).update(dataForUpdate).transacting(trx);
                queries.push(query);
              }
            }
          });

          Promise.all(queries) // Once every query is written
            .then(trx.commit) // We try to execute all of them
            .catch(trx.rollback); // And rollback in case any of them goes wrong
        });
      }

      /////////////////////////// МУСОРНЫЕ СИСТЕМЫ ////////////////////////////////
      const ttcGarbageSystemFromXLSX = XLSX.utils.sheet_to_json(workbook.Sheets.ttcGarbageSystem);
      let responseByTtcGarbageSystem = [];

      // проверка что есть данные на вкладке ttcGarbageSystem и что все колонки на месте
      if (ttcGarbageSystemFromXLSX.length > 0) {
        const validColumnsName = ['vendorCode', 'containersCount', 'depthBowl', 'facadeType', 'sizeCupboard'];
        const columnsNameFromTtcGarbageSystemFromXLSX = Object.keys(ttcGarbageSystemFromXLSX[0]);

        if (!_.isEqual(_.sortBy(validColumnsName), _.sortBy(columnsNameFromTtcGarbageSystemFromXLSX))) {
          return {
            error: 'XLSX list `ttcGarbageSystem` is not in the correct format',
          };
        }

        // массово обновляем данные
        responseByTtcGarbageSystem = await knex.transaction(trx => {
          const queries = [];

          productsVendorCodes.forEach(item => {
            const xlsxRow = ttcGarbageSystemFromXLSX.find(o => o['vendorCode'] === item.vendorCode);
            if (xlsxRow) {
              const productComponent = productsComponents.find(o => o.product_id === item.id && o.field === 'ttcGarbageSystem');
              if (productComponent) {
                const dataForUpdate = {};

                if (hasValue(xlsxRow['containersCount'])) {
                  dataForUpdate.containersCount = xlsxRow['containersCount'];
                }

                if (hasValue(xlsxRow['depthBowl'])) {
                  dataForUpdate.depthBowl = xlsxRow['depthBowl'];
                }

                if (hasValue(xlsxRow['facadeType'])) {
                  dataForUpdate.facadeType = xlsxRow['facadeType'];
                }

                if (hasValue(xlsxRow['sizeCupboard'])) {
                  dataForUpdate.sizeCupboard = Number(xlsxRow['sizeCupboard'].split(' ')[0]);
                }

                // This makes every update be in the same transaction
                const query = knex('components_tactical_technical_characteristics_garbage_systems')
                  .where('id', productComponent.component_id).update(dataForUpdate).transacting(trx);
                queries.push(query);
              }
            }
          });

          Promise.all(queries) // Once every query is written
            .then(trx.commit) // We try to execute all of them
            .catch(trx.rollback); // And rollback in case any of them goes wrong
        });
      }

      return {
        productСountOfTotal: responseByProduct.length,
        productСountOfUpdates: responseByProduct.filter(o => o === 1).length,
        productСountOfCanceles: responseByProduct.filter(o => o === 0).length,

        ttcBowlСountOfTotal: responseByTtcBowl.length,
        ttcBowlСountOfUpdates: responseByTtcBowl.filter(o => o === 1).length,
        ttcBowlСountOfCanceles: responseByTtcBowl.filter(o => o === 0).length,

        ttcMixerСountOfTotal: responseByTtcMixer.length,
        ttcMixerСountOfUpdates: responseByTtcMixer.filter(o => o === 1).length,
        ttcMixerСountOfCanceles: responseByTtcMixer.filter(o => o === 0).length,

        ttcGarbageSystemСountOfTotal: responseByTtcGarbageSystem.length,
        ttcGarbageSystemСountOfUpdates: responseByTtcGarbageSystem.filter(o => o === 1).length,
        ttcGarbageSystemСountOfCanceles: responseByTtcGarbageSystem.filter(o => o === 0).length,
      };
    } catch (error) {
      return { error };
    }
  },

  async downloadXLSX(context) {
    const filePath = __dirname + '/../../../public/import-template.xlsx';

    const workbook = await XLSXPopulate.fromFileAsync(filePath);

    // products
    // vendorCode	category	name	color	price	discountPercentage	designType	segment

    const knex = strapi.connections.default;
    const products = await knex('products')
      .leftJoin('categories', 'categories.id', 'products.category')
      .leftJoin('colors', 'colors.id', 'products.color')
      .select(
        'products.vendorCode',
        'products.name',
        'categories.id as categoryId',
        'categories.name as categoryName',
        'colors.id as colorId',
        'colors.name as colorName',
        'products.price',
        'products.discountPercentage',
        'products.designType',
        'products.segment'
      );

    // cформируем массив массивов
    const productsData = _.map(products, o => {
      // vendorCode: 512319,
      // name: 'ALTA',
      // categoryId: 3,
      // categoryName: 'Смеситель',
      // colorId: 1,
      // colorName: 'Смесители - хром',
      // price: 31000,
      // discountPercentage: 2.5,
      // designType: 'Modern',
      // segment: 'better'

      return [
        o.vendorCode,
        o.categoryId + ' ' + o.categoryName,
        o.name,
        o.colorId ? o.colorId + ' ' + o.colorName : '—',
        o.price,
        o.discountPercentage,
        o.designType,
        o.segment,
      ];
    });
    workbook.sheet('product').cell('A2').value(productsData);


    // ttcBowl
    // vendorCode	materialType	sizeCupboard	washingForm	bowlType	installationType	hasWing	hasDispenser	hasAutomaticValve	mixerSide

    const ttcBowls = await knex('components_tactical_technical_characteristics_bowls')
      .join('size_cupboards', 'size_cupboards.id', '=', 'components_tactical_technical_characteristics_bowls.sizeCupboard')
      .join('products_components', 'products_components.component_id', '=', 'components_tactical_technical_characteristics_bowls.id')
      .join('products', 'products.id', '=', 'products_components.product_id')
      .where('products_components.field', 'ttcBowl')
      .select(
        'products.vendorCode',
        'components_tactical_technical_characteristics_bowls.materialType',
        'size_cupboards.id as sizeCupboardId',
        'size_cupboards.size as sizeCupboardSize',
        'components_tactical_technical_characteristics_bowls.washingForm',
        'components_tactical_technical_characteristics_bowls.bowlType',
        'components_tactical_technical_characteristics_bowls.installationType',
        'components_tactical_technical_characteristics_bowls.hasWing',
        'components_tactical_technical_characteristics_bowls.hasDispenser',
        'components_tactical_technical_characteristics_bowls.hasAutomaticValve',
        'components_tactical_technical_characteristics_bowls.mixerSide',
      );

    // cформируем массив массивов
    const ttcBowlsData = _.map(ttcBowls, o => {
      return [
        o.vendorCode,
        o.materialType,
        o.sizeCupboardId + ' ' + o.sizeCupboardSize,
        o.washingForm,
        o.bowlType,
        o.installationType,
        o.hasWing ? 1 : 0,
        o.hasDispenser ? 1 : 0,
        o.hasAutomaticValve ? 1 : 0,
        o.mixerSide,
      ];
    });
    workbook.sheet('ttcBowl').cell('A2').value(ttcBowlsData);


    // ttcMixer
    // vendorCode	heightType	hasFilteredWater	hasPulloutSpout	hasFlexibleSpout	hasRemovableHose	hasShowerMode	hasWindowInstallation	hasTouchActivation	hasWaterMeasurement

    const ttcMixers = await knex('components_tactical_technical_characteristics_mixers')
      .join('products_components', 'products_components.component_id', '=', 'components_tactical_technical_characteristics_mixers.id')
      .join('products', 'products.id', '=', 'products_components.product_id')
      .where('products_components.field', 'ttcMixer')
      .select(
        'products.vendorCode',
        'components_tactical_technical_characteristics_mixers.heightType',
        'components_tactical_technical_characteristics_mixers.hasFilteredWater',
        'components_tactical_technical_characteristics_mixers.hasPulloutSpout',
        'components_tactical_technical_characteristics_mixers.hasFlexibleSpout',
        'components_tactical_technical_characteristics_mixers.hasRemovableHose',
        'components_tactical_technical_characteristics_mixers.hasShowerMode',
        'components_tactical_technical_characteristics_mixers.hasWindowInstallation',
        'components_tactical_technical_characteristics_mixers.hasTouchActivation',
        'components_tactical_technical_characteristics_mixers.hasWaterMeasurement',
      );

    // cформируем массив массивов
    const ttcMixersData = _.map(ttcMixers, o => {
      return [
        o.vendorCode,
        o.heightType,
        o.hasFilteredWater ? 1 : 0,
        o.hasPulloutSpout ? 1 : 0,
        o.hasFlexibleSpout ? 1 : 0,
        o.hasRemovableHose ? 1 : 0,
        o.hasShowerMode ? 1 : 0,
        o.hasWindowInstallation ? 1 : 0,
        o.hasTouchActivation ? 1 : 0,
        o.hasWaterMeasurement ? 1 : 0,
      ];
    });
    workbook.sheet('ttcMixer').cell('A2').value(ttcMixersData);


    // ttcGarbageSystem
    // vendorCode	containersCount	depthBowl	facadeType	sizeCupboard

    const ttcGarbageSystems = await knex('components_tactical_technical_characteristics_garbage_systems')
      .join('size_cupboards', 'size_cupboards.id', '=', 'components_tactical_technical_characteristics_garbage_systems.sizeCupboard')
      .join('products_components', 'products_components.component_id', '=', 'components_tactical_technical_characteristics_garbage_systems.id')
      .join('products', 'products.id', '=', 'products_components.product_id')
      .where('products_components.field', 'ttcGarbageSystem')
      .select(
        'products.vendorCode',
        'components_tactical_technical_characteristics_garbage_systems.containersCount',
        'components_tactical_technical_characteristics_garbage_systems.depthBowl',
        'components_tactical_technical_characteristics_garbage_systems.facadeType',
        'size_cupboards.id as sizeCupboardId',
        'size_cupboards.size as sizeCupboardSize',
      );

    // cформируем массив массивов
    const ttcGarbageSystemsData = _.map(ttcGarbageSystems, o => {
      return [
        o.vendorCode,
        o.containersCount,
        o.depthBowl,
        o.facadeType,
        o.sizeCupboardId + ' ' + o.sizeCupboardSize,
      ];
    });
    workbook.sheet('ttcGarbageSystem').cell('A2').value(ttcGarbageSystemsData);

    // отправляем файл
    context.set('Content-disposition', 'attachment; filename=import-filled-template.xlsx');
    context.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    // context.body = workbook.outputAsync();

    return workbook.outputAsync(); // здесь поидее должен быть await
  }
};
