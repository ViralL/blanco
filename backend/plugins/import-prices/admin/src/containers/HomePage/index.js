/*
 *
 * HomePage
 *
 */

import React, { memo, Component } from 'react';
import {
  request,
  HeaderNav,
  LoadingIndicator,
  PluginHeader
} from 'strapi-helper-plugin';
// import PropTypes from 'prop-types';
// import pluginId from '../../pluginId';
import Row from '../../components/Row';
import Block from '../../components/Block';
import UploadFileForm from '../../components/UploadFileForm';
// import { Select, Label } from '@buffetjs/core';

class HomePage extends Component {
  state = {
    analyzing: false,
    analysis: null
  };

  onRequest = async config => {
    this.setState({ analysis: null, analyzing: true }, async () => {
      try {
        const response = await request('/import-prices/upload', {
          method: 'POST',
          body: config,
        });

        if (!response.error) {
          this.setState({ analysis: response, analyzing: false }, () => {
            strapi.notification.toggle({
              type: 'success',
              message: 'Analyzed Successfully',
              timeout: 10000,
            });
          });
        } else {
          this.setState({ analyzing: false }, () => {
            strapi.notification.toggle({
              type: 'warning',
              title: 'Analyze Failed, try again',
              message: `${response.error}`,
              timeout: 5000,
            });
          });
        }
      } catch (e) {
        this.setState({ analyzing: false }, () => {
          strapi.notification.toggle({
            type: 'warning',
            title: 'Analyze Failed, try again',
            message: `${e}`,
            timeout: 5000,
          });
        });
      }
    });
  };

  render() {
    return <div className={'container-fluid'} style={{ padding: '18px 30px' }}>
      <PluginHeader
        title="Import Data"
        description="Import data by vendor codes, this plugin not create product, only edit exist products"
      />

      <div className="row">
        <Block
          style={{ marginBottom: 12 }}
        >
          <a href="/import-template.xlsx" target="_blank">Download XLSX-template file</a><br />
          <a href="/import-prices/download-xlsx" target="_blank">Download <strong>filled</strong> XLSX-template file</a>
          <UploadFileForm
            onRequest={this.onRequest}
            analyzing={this.state.analyzing}
          />
        </Block>
      </div>

      {
        this.state.analysis &&
        <div>
          <div className="row">
            <Block
              title="Результат обработки вкладки product"
              description={'Всего строк: ' + this.state.analysis.productСountOfTotal}
              style={{ marginBottom: 12 }}
            >
              <p>Из них успешно обновлено: {this.state.analysis.productСountOfUpdates}</p>
              <p>Из них пропущено с ошибкой: {this.state.analysis.productСountOfCanceles}</p>
            </Block>
          </div>
          <div className="row">
            <Block
              title="Результат обработки вкладки ttcBowl"
              description={'Всего строк: ' + this.state.analysis.ttcBowlСountOfTotal}
              style={{ marginBottom: 12 }}
            >
              <p>Из них успешно обновлено: {this.state.analysis.ttcBowlСountOfUpdates}</p>
              <p>Из них пропущено с ошибкой: {this.state.analysis.ttcBowlСountOfCanceles}</p>
            </Block>
          </div>
          <div className="row">
            <Block
              title="Результат обработки вкладки ttcMixer"
              description={'Всего строк: ' + this.state.analysis.ttcMixerСountOfTotal}
              style={{ marginBottom: 12 }}
            >
              <p>Из них успешно обновлено: {this.state.analysis.ttcMixerСountOfUpdates}</p>
              <p>Из них пропущено с ошибкой: {this.state.analysis.ttcMixerСountOfCanceles}</p>
            </Block>
          </div>
          <div className="row">
            <Block
              title="Результат обработки вкладки ttcGarbageSystem"
              description={'Всего строк: ' + this.state.analysis.ttcGarbageSystemСountOfTotal}
              style={{ marginBottom: 12 }}
            >
              <p>Из них успешно обновлено: {this.state.analysis.ttcGarbageSystemСountOfUpdates}</p>
              <p>Из них пропущено с ошибкой: {this.state.analysis.ttcGarbageSystemСountOfCanceles}</p>
            </Block>
          </div>
        </div>
      }
    </div>;
  }
}

export default memo(HomePage);
