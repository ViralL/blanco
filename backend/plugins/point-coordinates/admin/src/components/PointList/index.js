import React, { useState } from 'react';
import {
  // useStrapi,
  prefixFileUrlWithBackendUrl,
  useContentManagerEditViewDataManager,
} from 'strapi-helper-plugin';
import { Input } from './Input';
import { LassoModal } from './Modal';

export default function PointList (props) {
  let linkedMediaUrl = ''; // путь к картинке

  // получим все данные данной модели (в нашем случае товара)
  const { modifiedData } = useContentManagerEditViewDataManager(); // , initialData

  // если картинка уже есть
  if (modifiedData.modelPictures) {
    const fieldPictureName = props.name.replace('Coordinates', '');
    const fieldsNames = fieldPictureName.split('.');

    if (fieldsNames.length > 1 && modifiedData.modelPictures[fieldsNames[1]]?.url) {
      const url = modifiedData.modelPictures[fieldsNames[1]].url;
      linkedMediaUrl = prefixFileUrlWithBackendUrl(url);
    }
  }

  // const { strapi } = useStrapi();

  const [isLassoModalOpen, setLassoModalOpen] = useState(false); // признак открытия попапа

  return (
    <div>
      <Input
        name={props.name}
        label={props.label}
        inputDescription={props.inputDescription}
        error={props.error}
        autoFocus={props.autoFocus}
        disabled={true}
        placeholder={props.placeholder}
        onBlur={props.onBlur}
        validations={{
          regex: /^(((\d+,\d+) )*(\d+,\d+))$|^$/
        }}
        value={props.value || ''}
        onClick={() => {
          // здесь по хорошему написать проверку на наличие linkedMediaUrl, и если его нет, то попытаться забрать из modifiedData
          if (linkedMediaUrl) {
            setLassoModalOpen(true);
          } else {
            strapi.notification.toggle({
              type: 'warning',
              title: 'Warning',
              message: 'Please, upload image',
              timeout: 5000,
            });
          }
        }}
      />
      <LassoModal
        isOpen={isLassoModalOpen}
        onToggle={() => {
          setLassoModalOpen(!isLassoModalOpen);
        }}
        src={linkedMediaUrl}
        value={props.value || ''}
        onConfirm={(value) => {
          props.onChange({
            target: {
              value,
              name: props.name,
              type: 'text'
            }
          });
        }}
      />
    </div>
  );
}