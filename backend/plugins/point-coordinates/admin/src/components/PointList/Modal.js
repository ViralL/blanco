import React, { useState, useEffect } from 'react';

import {
  Modal,
  ModalHeader,
  ModalFooter,
  ButtonModal,
  ModalBody as modalBody,
} from 'strapi-helper-plugin';
import styled from 'styled-components';

const ModalBody = styled(modalBody)`
  .row {
    justify-content: center;
  }
`;

const ImageContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 890px;
  height: 890px;
  outline: 1px solid rgba(0, 0, 0, 0.1);

  > img {
    outline: 1px solid rgba(0, 0, 0, 0.1);
  }
`;

const Point = styled.p`
  position: absolute;
  top: 0;
  left: 0;
  width: 6px;
  height: 6px;
  border: 1px solid red;
  border-radius: 2px;
  background: rgba(255, 0, 0, 0.2);
  transform: translate3d(-3px, -3px, 0);
  margin: 0;
  pointer-events: none;
`;

const pathToString = (arr) => arr.map(({x,y}) => `${x},${y}`).join(' ');
const pathFromString = (str) => str.split(' ').filter(Boolean).map(s => s.split(',').map(Number)).map(([x,y]) => ({x, y}));

export function LassoModal ({ isOpen, src, value, onConfirm, onDismiss, onToggle }) {
  const [lassoValue, setLassoValue] = useState(pathFromString(value));
  const [pointDeltaX, setPointDeltaX] = useState(0);
  const [pointDeltaY, setPointDeltaY] = useState(0);
  const [scale, setScale] = useState(1);

  const imageContainerRef = React.createRef();

  // // следим за изменениями в isOpen, чтобы обновить координаты
  // useEffect(() => {
  //   if (isOpen) {
  //   }
  // }, [isOpen]);

  // загружаем картинку чтобы понимать пропорции
  const onImageLoad = () => {
    const parentNode = imageContainerRef.current;
    const parentCoords = parentNode.getBoundingClientRect();

    const imageNode = parentNode.querySelector('img');
    const imageCoords = imageNode.getBoundingClientRect();

    setPointDeltaX(imageCoords.left - parentCoords.left);
    setPointDeltaY(imageCoords.top - parentCoords.top);

    // расчитываем пропорции/маштаб картинки
    const newScale = imageNode.width / imageNode.naturalWidth;
    setScale(newScale);
  };
  
  const onClickImage = (event) => {
    const realEvent = event.nativeEvent;

    const imageNode = realEvent.target;
    const imageCoords = imageNode.getBoundingClientRect();
    
    const parentNode = imageNode.parentNode;
    const parentCoords = parentNode.getBoundingClientRect();

    let x = realEvent.clientX - parentCoords.left;
    let y = realEvent.clientY - parentCoords.top;

    x -= imageCoords.left - parentCoords.left;
    y -= imageCoords.top - parentCoords.top;

    // масштаб
    x /= scale;
    y /= scale;

    x = parseInt(x, 10);
    y = parseInt(y, 10);

    setLassoValue([{ x, y }]);
  };

  return (
    <Modal isOpen={isOpen} onToggle={onToggle}>
      <ModalHeader
        onClickGoBack={onToggle}
        HeaderComponent={() => <p>Select point by clicking on image</p>}
      />
      <ModalBody style>
        <ImageContainer ref={imageContainerRef}>
          <img
            src={src}
            style={{
              maxWidth: '100%',
              maxHeight: '100%',
              cursor: 'pointer',
            }}
            onClick={onClickImage}
            onLoad={onImageLoad}
          />
          {
            lassoValue[0] && <Point
              style={{
                top: ((lassoValue[0].y + pointDeltaY) * scale) + 'px',
                left: ((lassoValue[0].x + pointDeltaX) * scale) + 'px',
              }}
            />
          }
        </ImageContainer>
      </ModalBody>
      <ModalFooter>
        <section>
          <ButtonModal message="app.components.Button.cancel" isSecondary={true} onClick={() => {
            onToggle();
            onDismiss();
          }}/>
          <ButtonModal message="app.components.Button.save" isSecondary={false} onClick={() => {
            onToggle();
            onConfirm(pathToString(lassoValue));
          }}/>
        </section>
      </ModalFooter>
    </Modal>
  );
}

LassoModal.defaultProps = {
  onConfirm: () => {},
  onDismiss: () => {}
};
